'''

@author: Frederik Claus <f.v.claus@googlemail.com>
'''


# use django-admin.py COMMAND --settings=settings_prod --insecure instead of anything else
# --insecure enables static file serving

from settings import *

DEBUG = False
TEMPLATE_DEBUG = False

temp_app = tuple()

for app in INSTALLED_APPS:
    if not app == "django.contrib.admin":
        temp_app += (app,)

INSTALLED_APPS = temp_app

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'videotagging',  # Or path to database file if using sqlite3.
        'USER': 'django',  # Not used with sqlite3.
        'PASSWORD': 'django',  # Not used with sqlite3.
        'HOST': 'localhost',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',  # Set to empty string for default. Not used with sqlite3.
        }
 }