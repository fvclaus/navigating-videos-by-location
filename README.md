Navigating videos by location
==================================================

This projects is an implementation of the paper [Navigating videos by location](http://ls.wim.uni-mannheim.de/de/pi4/people/philip-mildner/publications/?tx_fmipublications_pi1[showUid]=911&tx_fmipublications_pi1[pointer]=0&tx_fmipublications_pi1[mode]=1&tx_fmipublications_pi1[sort]=uid%3A1&cHash=746dfa65a0794b040d02f6be99a4800e). It takes videos + gpx traces as input and renders the videos on a map where they can be navigated by location and automatically chained together. It is implemented as a Python Django app and requires very few external dependencies.

Behind the scenes the gpx trace will be map-matched on a road network (currently: OSM). The best starting point for the map-matching is geo.core.mapmatching.py. Roughly speaking the map-matching process works like this: After an inital start matching which compares the heading the heading of single roads, a topological view is created, where every new point can only be matched to a neighbouring road.
Because of the high density of urban road networks, several algorithms to improve the accuraccy of the matching process have been implemented.
Among them are a backjumping algorithm that will detect and correct mistakes that have been made with previous points and a weighted directional comparison that compares the heading and distance of points to roads of the network.

Map-matched traces are stored in a datastructure that defines MapPoints (points from a GIS), MapPointConnection (also from a GIS), TracePoints and TracePointConnection.
TracePoints are MapPoints that have been visited in a single trace. TracePointConnection is the connection between two TracePoints.

On the defined data structure, it is possible to apply path finding algorithms, like Dijkstra single source shortest path or A*.
The main difference between classical graphs defined like <V,E> is that there can be several edges between two vertices.
Traditional path finding algorithm do not take this in account and produce an ambigous result. Therefore a variaton of the single source shortest path algorith has been developed that iterates over edges instead of vertices and stores meta-data also on edges.

Setup
--------------------------------------

REQUIREMENTS: These instructions have been tested and confirmed with Ubuntu. Although they should work with any other Linux distribution, you should check back with your system first.

If you want to use a virtualenv, you must make sure that your mod_wsgi is compiled against the same python verison. If it is not, don't use one. I recommend trying virtualenv first because it is the cleaner way.

Frontend
--------------------------------------

**IMPORTANT**: Use these instructions if you are not working on either the map-matching algorithm nor the routing algorithm.

- Get the videos.zip from [here](http://www.wuala.com/fclaus/public/hiwi/videos.zip/) and extract them to videos/
- Install necessary dependencies `(sudo) pip install -r install/requirements.text` with pip. These requirements should be pure python modules. No native code required.
- Test dependencies with `python manage.py test geo`. If it does not say 'Ran XX test in XX secs. OK', some dependencies are still missing.
- To add some data to the system you have two choices:
	1. Run the setup script, `python setup.py`. The setup script will take the sqlite3 db from res/ and update the path to the videos, which are copied from videos/ to /static/upload. The prebuilt sqlite database might be outdated, but is fast to setup.
	2. Run `python manage.py syncb` and `python manage.py videotagging --match --insert --input-traces res/tracks --input-videos videos/ --input-video-extension .ogv`. This will match all traces and insert them with the correct video. 
- You can start django now `python manage.py runserver`. The service should run without problems. If it still does not work, write me an email so I can add instructions here. Browse to localhost:8000/index.

Django does not support a byte range request, which is necessary for streaming videos correctly and determining the position in the video. You'll notice this, when you playback a video and look at the current position in seconds and the total seconds. I will explain a setup for apache which supports byte range requests. If you want to use another webserver feel free to choose whichever one you like.
- Install `apache2` & and `libapache2-mod-wsgi` (*Note*: The exact names might be different on your platform). If you can't or don't want to install mod_wsgi you can also use mod_python. mod_python does not support the selection of a different python interpreter, mod_wsgi does under certain circumstances (see above)).
- Go to the install/ folder open the httpd.conf, copy it to /etc/apache2/ and adjust all paths to fit your local setup. PLEASE make sure you changed all of your pathes. In addition to that you will need a emtpy virtualhost:80 configuration in sites-enabled that just contains a ServerRoot directive, don't forget to raise the debug level. If you used virtualenv in one of the steps above add WSGIPYTHONHOME to your httpd.conf, for details see the [virtualenv section of the mod-wsgi documentation](http://code.google.com/p/modwsgi/wiki/VirtualEnvironments).
*NOTE*: With debian the httpd.conf should be placed in /etc/apache2/conf.d/ as videotagging.local.conf. Check your local apache2 installation if you use another distribution.
- Change the permisson of the static/ folder to global read.
- Restart your apache and browser to localhost/index, you should see the app running again. If you select a video now, you will notice that the total time in seconds is in a more believable range than earlier. If you see the blue marker highlighting the current section your done! If you get some error (Acess denied, etc..), the page comes without styling something went wrong. Consult your apache log.

Backend
--------------------------------------

**IMPORTANT**: Use these instructions if you are working on the map-matching algorithm or the routing algorithm, but not on the frontend. This requires libraries for running PostgreSQL and matplotlib to handle batch insertion of several hundred points and plotting this as raster image.

- Install necessary dependencies `(sudo) pip install -r install/requirements.text` with pip. Optionally, you can install the requirements in requirements_optional.txt. They will add functionality to plot the graph as raster image. Some modules contain native code and require compilation.
- Test dependencies with `python manage.py test geo`. If it does not say 'Ran XX test in XX secs. OK', some dependencies are still missing.
- There is no need to download any videos because they won't playback correctly anyway (read about byte range issues in the frontend setup section). You can now start to use the command facility `python manage.py videotagging --help`. It includes commands to match a single trace or a whole folder, to insert a map-matched file or a whole folder of map-matched files or calculate the shortest path between two points on the graph data in the db. There are many options, please refer to the --help switch.
- A good workflow seems to be the use of --match --print-ids and setting breakpoints in a debugger and stepping through the code.

*NOTE*: You might see some errors, because /static/trace cannot be resolved. This path is actually rewritten in httpd.conf for apache2 and app.yaml for appengine. AFAIK this is not easy to do with Django.
Production
--------------------------------------

The project can either be deployed on apache2 or any other webserver or google appengine. Google appengine is a bit special and requires the use of the django non-rel package. Here are the instructions: [djangoappengine](djangoappengine.readthedocs.org/en/latest/installation.html)
apache2 deployment works as described in the Backend section.

Useful links
--------------------------------------

Here are some useful links for working with the project:
- [GPX](http://en.wikipedia.org/wiki/Gpx)
- [OSM: List of element tags](http://wiki.openstreetmap.org/wiki/Map_Features)
- [OSM: History of node changes (example link)](http://api.openstreetmap.org/api/0.6/node/1756280956/history)
- [OpenLayers](http://docs.openlayers.org/)
- [JOSM](http://wiki.openstreetmap.org/wiki/Josm)
