from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.views.generic.simple import redirect_to


from geo.views import index, trace, wikipedia

urlpatterns = patterns('',
                       url(r'^$', redirect_to, {"url" : "index"}),
                       (r'^index$', index),
                       (r'^trace/$', trace),
                       (r'^wikipedia/(?P<title>[^/]+)/$', wikipedia),
                       (r'^wikipedia/(?P<title>[^/]+)/(?P<language>[a-z]{2})/$', wikipedia),
)


if settings.DEBUG:
    from django.contrib import admin
    admin.autodiscover()
    urlpatterns += patterns("",
                            url(r'^admin/', include(admin.site.urls)),)
