/*global $, _V_, OpenLayers, MapLayer, styles, assert, assertTrue, geographic, onVideoProgress, Trace, TracePoint, TraceConnection, markerLayer */

"use strict";

/**
 * @public
 * @author Frederik Claus <f.v.claus@googlemail.com>
 * @description Displays a trace between two points on the graph and hightlights the progress.
 * @param {OpenLayers.Map} map
 */
var TraceLayer = function(map){
   assert(map.CLASS_NAME, "OpenLayers.Map");

   var instance = this,
       $video = $("#video"),
       width = $video.width(),
       height = $video.height();

   this.map  = map;
   this.player =  _V_("video");
   //mute player
   this.player.volume(0);
   this.player.size(width,height);
   this._bindListenerToVideo();


   this.traceLayer = null;

   this.oldFeature = undefined;
   this.newFeature = undefined;

   assertTrue(typeof styles.track === "object");
   assertTrue(typeof styles.active === "object");

   this.traceLayer  = new OpenLayers.Layer.Vector("TrackLayer",{
      styleMap : new OpenLayers.StyleMap({
	 "default" : styles.track,
	 "select" : styles.active
      }),
      projection  : geographic
   });

   this._initControls();
};

TraceLayer.prototype = {
   /**
    * @public
    * @description Removes the trace layer from the graph layer
    */
   destroy : function(){
      this._stopHighlight();

      if (this.traceLayer){
	 this.map.removeLayer(this.traceLayer);
         this.traceLayer.removeAllFeatures();
	 // this.traceLayer = null;
      }
   },
   /**
    * @public
    * @description Reinitialises the layer and draws the points
    */
   update : function(points){
      assertTrue(points instanceof Array);
      assertTrue(points.length > 0);
      
      var source = null,
          trace = new Trace(),
          features = [];
     
      // build the new trace
      points.forEach(function(point){
	 trace.add(point);
      });
      
      // draw the trace on the layer
      this.traceLayer.addFeatures(trace.toFeatures());
      // add the trace-layer to the map
      this.trace = trace;
      // show layer
      this.map.addLayer(this.traceLayer);
      // if we don't set the layerindex to a high number, it will be displayed below the map layer
      this.map.setLayerIndex(this.traceLayer,999);
      // it is only possible to set the z-index of the whole layer after adding it
      // it must be > 726, the z-index of the highest vector layer
      markerLayer.setZIndex(750);

      this._startVideo();
   },
   /**
    * @public
    * @returns {OpenLayers.Layer.Vector} Layer that displays the shortest trace
    */
   getVectorLayer : function () {
      assertTrue(this.traceLayer !== undefined);
      return this.traceLayer;
   },
   /** 
    * @private
    * @description Starts the video belonging to the current active segment
    */
   _startVideo : function(feature){
      var start = this.trace.getActive();
      this.player.pause();

      this._stopHighlight();

      this.player.src({type:"video/ogg",src:start.getData("src")});

      this._bindListenerToVideo();

      console.log("start at %d with %s",start.getData("videotimestart"),start);
   },
   /*
    * @private
    * @description Start playing as soon as the video metadata is fully loaded and update the current position of the trace.
    */
   _bindListenerToVideo : function () {
      var instance = this;
      // This will never be triggered, if the webserver does not support byte range requests.
      this.player.addEvent("loadeddata",function(){
         // 'this' is the video object
         this.addEvent("timeupdate", function () {
            instance._highlightUpdate.apply(instance);
         });
         this.currentTime(instance.trace.getActive().getData("videotimestart"));
         this.play();
      });
   },
   /** 
    * @private
    * @description Removes the highlight listener from the video
    */
   _stopHighlight : function(){
      this.player.removeEvent("timeupdate",this._highlightUpdate);
   },
   /** 
    * @private
    * @description Highlight the current segment of the trace-layer
    */
   _highlightUpdate : function(){
      assertTrue(this instanceof TraceLayer);

      // Returns the next active feature of the trace.
      // This might be a trace belonging to a different source.
      this.newFeature  = this.trace.proceed(this.player.currentTime());

      // Inform others about the progress.
      if (this.oldFeature !== this.newFeature && this.newFeature !== null) {
         var next = this.trace.getNext(),
             pointToConnection = this.newFeature instanceof TracePoint && next instanceof TraceConnection,
             connectionToPoint = this.newFeature instanceof TraceConnection && next instanceof TracePoint,
             segmentChange = this.newFeature instanceof TracePoint && next instanceof TracePoint,
             ended = this.newFeature instanceof TracePoint && next === null;

         assertTrue(pointToConnection || connectionToPoint || segmentChange || ended);

         onVideoProgress(this.newFeature, next);
      }
      // End of video
      // Don't remove listener yet -- this will be done in the layer destroy.
      if (this.newFeature === null){
	 if (!this.isEnded){
	    this._removeHighlight();
	 }
	 console.log("TraceLayer: ended");
	 return;
      } else if (this.oldFeature === undefined) {
         this._highlightFeature();
         console.log("TraceLayer: start");
      } else if (this.oldFeature.getData("src") !== this.newFeature.getData("src")) { //change of segments
	 console.log("TraceLayer: new segment");
	 this.player.pause();
	 this._stopHighlight();
	 // Restart video
	 this._startVideo();
         // Avoid the video from being stuck at a transition.
         this.oldFeature = this.newFeature;
	 return;
      }
      // Video proceeds, highlight current feature.
      if (this.oldFeature !== this.newFeature){
	 console.log("TraceLayer: new feature");
	 this._highlightFeature();
      }
      this.oldFeature = this.newFeature;
   },
   /**
    * @private
    * @description Highlights the current visited segment (Point or Connection) of the trace
    */
   _highlightFeature : function(){
      this._removeHighlight();
      this.ignoreSelect = true;
      this.highlight.select(this.trace.getActive().getVector());
      this.ignoreSelect = false;
      this.isEnded = false;
   },
   /**
    * @private
    * @description Remove the highlight from the layer.
    */
   _removeHighlight : function(){
      this.highlight.unselectAll();
      this.isEnded = true;
   },
   /**
    * @private
    */
   _initControls : function(){
      assert(this.traceLayer.CLASS_NAME, "OpenLayers.Layer.Vector");
      var instance = this;

      if (this.highlight){
	 this.map.removeControl(this.highlight);
      }

      // this will only hightlight the trace
      // the highlight styles are defined in the traceLayer constructor
      this.highlight = new OpenLayers.Control.SelectFeature(this.traceLayer,{
	 multiple : false,
	 clickout : false,
	 toggle : false,
	 highlightOnly : true,
      });

      if (this.select){
	 this.map.removeControl(this.select);
      }
      // add a navigation selection feature control
      this.select = new OpenLayers.Control.SelectFeature(this.traceLayer,{
	 multiple : false,
	 clickout : false,
	 toggle : false,
	 highlightOnly : true,
	 onSelect : function(feature){
	    //triggered by user
	    if (!instance.ignoreSelect){
               
               if (feature.geo_data === undefined) {
                  console.log("The selected feature does not belong to the graph. Skipping...");
                  return;
               }

	       var srcOld = instance.trace.getActive().getData("src"),
	           src = feature.geo_data.src,
	           videotimestart = feature.geo_data.videotimestart;

	       // hit a mapconnection do nothing
	       if (!src){
		  alert("There is no video to show here.");
	       }
	       // hit another video. set active and load video
	       else if (srcOld !== src){
		  instance._stopHighlight();
		  instance.trace.setActive(src, feature.geo_data.id);
		  instance._startVideo();
	       }
	       // still the same video. proceed to the current time marker
	       else{
		  instance.player.currentTime(videotimestart);
	       }
	    }			
	 }
      });

      
      this.map.addControl(this.highlight);
      this.map.addControl(this.select);
      this.highlight.activate();
      this.select.activate();
   },
};
