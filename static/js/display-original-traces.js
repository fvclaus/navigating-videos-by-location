/*global document, OpenLayers, $, geographic, spherical, location */
"use strict";

/* Displays the unmatched (original) traces on a seperate map. */

// Coordinates of the approximate center of the traces
var centerLat = 49.487689,
    centerLon = 8.466232,
    centerZoom = 16,
// File names of the traces that should be displayed.
// All traces must be accessible via /static/traces/[name].gpx.
    TRACES_FORTH = ["Track201210311143", 
                       "Track201210311152", 
                       "Track201210311207", 
                       "Track201210311325_2",
                       "Track201210311340",
                       "Track201210311348"],
   TRACES_BACK = ["Track201210311219",
                  "Track201210311214",
                  "Track201210311312",
                  "Track201210311343",
                  "Track201210311354",
                  "Track201210311405",
                  "Track201210311413",
                  "Track201210311421",
                  "Track201210311435"];


function createGPXLayer(name, color) {
   return new OpenLayers.Layer.Vector(name, {
      strategies: [new OpenLayers.Strategy.Fixed()],
      protocol: new OpenLayers.Protocol.HTTP({
         url: "static/trace/" + name + ".gpx",
         format: new OpenLayers.Format.GPX()
      }),
      style: {strokeColor: color, strokeWidth: 5, strokeOpacity: 0.8},
      projection: geographic
   });
}

function createGPXLayers () {
   var traces = [];
   TRACES_FORTH.forEach(function (trace) {
      traces.push(createGPXLayer(trace, "green"));
   });
   TRACES_BACK.forEach(function (trace) {
      traces.push(createGPXLayer(trace, "blue"));
   });
   return traces;
}

$(document).ready(function () {
   var map = new OpenLayers.Map ({
      div: "map-original",
      controls:[new OpenLayers.Control.Navigation()]
   }),
       layerMapnik = new OpenLayers.Layer.OSM.Mapnik("Mapnik"),
       layerCycleMap = new OpenLayers.Layer.OSM.CycleMap("CycleMap"),      
       layerMarkers = new OpenLayers.Layer.Markers("Markers"),
       lonLat = new OpenLayers.LonLat(centerLon, centerLat)
          .transform(geographic, spherical),
       traceLayers = createGPXLayers();

   map.addLayer(layerMapnik);
   traceLayers.forEach(function (layer) {
      map.addLayer(layer);
   });
   map.setCenter(lonLat, centerZoom);
});
