'''
Automate common tasks like map-matching a single trace or a folder of traces.
Supports insertion map-matched traces in the database and drawing the a certain graph as png image.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

from optparse import make_option
import os
import glob
import pickle

from django.core.management.base import NoArgsCommand, CommandError
from django.conf import settings

from geo.io.gpx import GPX

from geo.models import createvideo
from geo.core.mapmatching import OSM
from geo.core.routing import Graph
from geo.base.util import TracePointDTO

#===============================================================================
# Helper functions
#===============================================================================
TRACK_DIR = os.path.join(settings.RES_DIR, "tracks")
VIDEO_DIR = settings.VIDEO_DIR
TEST_VIDEO_PATH = os.path.join(settings.TEST_PATH, "video.3gp")

def map_match_file(path, mode, suffix = ".geocode", interpolation_factor = 0):
    """Map match one file and store the result in a file with the given extension."""
    assert os.path.isfile(path)
    gpx = GPX(open(path, "r"))
    geocoder = OSM(mode)

    if not gpx.is_valid():
        raise RuntimeError, "gpx file is not valid"
    coordinates = gpx.get_track_points()
    geocodes = geocoder.match(coordinates, interpolation_factor)
    
    out = open(os.path.splitext(path)[0] + suffix, "w")
#    pickle.dump(video_times(geocodes), out)
    pickle.dump(geocodes, out)
    out.flush()
    out.close()
    
def geocode_to_dto(geocode_file, videopath):
    """Reads geocode geocode_file and returns a parsed copy."""
    dtos = []
    video = createvideo(videopath)
    for (lat, lon, real_time, video_time) in video_times(pickle.load(geocode_file)):
        dto = TracePointDTO(lat = lat, lon = lon, real_time = real_time, video_time = video_time, video = video)
        dtos.append(dto)
    return dtos

def get_ids(geocode_path):
    geocode_file = open(geocode_path, "r")
    unique_ids = []
    for (lat, lon, real_time, node_id) in pickle.load(geocode_file):
        if len(unique_ids) == 0 or node_id != unique_ids[-1]:
            unique_ids.append(node_id)
    return unique_ids
    
def insert_geocodes(trace_paths, videos_path, mode, video_extension = None):
    """Inserts geocode file into graph with specified mode."""
    assert os.path.exists(videos_path), "Videos_path needs to be a legal path."
    graph = Graph.get_instance(mode)
    
    for path in trace_paths:
        assert os.path.exists(path), "Path %s needs to be a legal path." % path
            
        if os.path.isfile(path):
            # Generate the path of the video for the trace
            # There is only one video file. Use it.
            if os.path.isfile(videos_path):
                video_path = videos_path
            # There are several video files. Select the one that matches the file name of the trace.
            elif os.path.isdir(videos_path):
                file_name = os.path.split(path)[1]
                assert not video_extension is None, "Must specify the video file extension when adding videos from folder."
                video_path = os.path.join(videos_path, os.path.splitext(file_name)[0] + video_extension)
                if not os.path.exists(video_path):
                    raise CommandError, "Can not find video for trace %s. Tried %s" % (file_name, video_path)
                
            dtos = geocode_to_dto(open(path, "r"), video_path)
            graph.insert_trace(dtos)
        else:
            print "Skipping %d. Reason: Not a file." % path
    
def calculate_shortest_path(source_id, target_id, mode):
    graph = Graph.get_instance(mode)
    source = graph.get_mappoint(pk = source_id)
    target = graph.get_mappoint(pk = target_id)
    
    if source is None:
        raise CommandError("MapPoint %d does not exist in graph %d." % (source_id, mode))
    if target is None:
        raise CommandError("MapPoint %d does not exist in graph %d." % (target_id, mode))
    
    shortest_path = graph.calculate_shortest_path(source, target)
    print shortest_path
    return shortest_path.as_json()
    
    
def draw_map(mode, filename = None):
    graph = Graph.get_instance(mode)
    
    if filename is None:
        filename = "%d" % mode
        
    graph.draw(os.path.join(settings.OUT_DIR, filename), reset = False)
    
def video_times(geocodes):
    video_times = []
    start = None
    for (lat, lon, real_time, id) in geocodes:
        if not start:
            start = real_time
            video_time = 0
        else:
            td = real_time - start
            video_time = td.seconds
        video_times.append((lat, lon, real_time, video_time))
    return video_times

#===============================================================================
# Expose helper functions to the command line.
#===============================================================================
class Command(NoArgsCommand):
    help = "Map-match a .gpx trace or a directory with gpx traces.\n" + \
            "Insert a .geocode file or a directory with .geocode files.\n" + \
            "Operations can be chained together, like --match --insert --draw-graph. Obey the requirements of the first operation.\n" + \
            "NOTE: Currently every trace will be assigned the same dummy video defined in settings.py upon insertion.\n" + \
            "This behavior can be changed using the --input-videos switch."
    option_list = NoArgsCommand.option_list + (
        make_option('--match', default = False, action = 'store_true', dest = "match",
                    help = 
                    "Map-match the trace(s) file(s) and store the result in a .geocode file." 
                    " The .geocode format stores the map-matched coordinates including the time when the coordinate was visited." 
                    " Video times are calculated when the trace is inserted into the database."),
        make_option("--interpolate", default = 0, action = "store", dest = "interpolate",
                    help = 
                    "Set the interpolation factor for the map-matching."
                    " If interpolation factor is bigger than 0, interpolation will add new coordinates for every connection."
                    " This is sometimes necessary when there are very few coordinates on a dense urban map."
                    " Otherwise the topological matching cannot keep up with the trace speed and will match the trace incorrectly."),
        make_option("--print-ids", default = False, action = "store_true", dest = "print_ids",
                    help = 
                    "Print a list of unique matched node ids."
                    " This is useful for development to compare matched ids with tools like JOSM."
                    " Needs to be used with --match or --input-traces to .geocode file"),
        make_option('--insert', default = False, action = 'store_true', dest = "insert",
                    help = 
                    "Insert map-matched .geocode trace(s) into database."
                    " Currently, there is no possiblity to connect a .geocode file with a video file."
                    " Therefore all traces will be assigned the same video defined in settings.py."),
        make_option('--draw-graph', default = False, action = 'store_true', dest = "draw_graph",
                    help = 'Draw graph with the selected mode as a MapPoint/Connection only and TracePoint/Connection only png image'),
        make_option("--print-shortest-path", default = False, action = "store_true", dest = "shortest_path",
                    help = 
                    "Calculate the shortest point between two points print the path."
                    " Specify the source with --source and the target with --target."),
        make_option("--source", action = "store", dest = "source",
                    help = "Id of the source MapPoint for the shortest path search."),
        make_option("--target", action = "store", dest = "target",
                    help = "If of the target MapPoint for the shortest path search."),
        make_option("--mode", default = 0, action = "store", dest = "mode",
                    help = 
                    "Select the mode for the graph. NOTE: This has influence on the map-matching as well."
                    " The mode also selects the graph the traces are inserted. Defaults to 0. Mode must be an integer."),
        make_option("--input-traces", action = "store", dest = "traces_path",
                    help = "Specify a file or folder. This is needed for --match and --insert."),
        make_option("--input-videos", default = TEST_VIDEO_PATH, action = "store", dest = "videos_path",
                    help = 
                    "Specify a file or folder." + \
                    "Defaults to a dummy video file."
                    "If input is file, the video will be assigned to all traces." + \
                    "If input is folder, every trace will be assigned the video with the same file name."),
        make_option("--input-videos-extension", default = None, action = "store", dest = "videos_extension",
                    help = 
                    "If selection a folder with --input-videos, it is also necessary to specify the extension of the videos." + \
                    "The videos get matched to the traces by matching the filename plus the correct extension." + \
                    "Specify the extension like this: .ogv")
                    )
    
    def handle_noargs(self, **options):
        match = options.get("match")
        insert = options.get("insert")
        draw = options.get("draw_graph")
        print_ids = options.get("print_ids")
        shortest_path = options.get("shortest_path")
        traces_path = options.get("traces_path")
        videos_path = options.get("videos_path")
        videos_extension = options.get("videos_extension")
        no_action_selected = not match and  not insert and not draw and not shortest_path and not print_ids
        needs_input = match or insert or (not match and print_ids)
        needs_extension = insert and os.path.isdir(videos_path)
        #=======================================================================
        # Check arguments
        #=======================================================================
        if no_action_selected:
            raise CommandError(
                               "Must specify at least one action of match, insert or draw-graph."
                               " Make sure to specify the options before(!) the input files.")
        if needs_input:
            if not traces_path:
                raise CommandError("Must use --insert or --match with --input-traces.")
                    
            if not os.path.exists(traces_path):
                raise CommandError("%s is not a valid traces_path to a file or directory." % traces_path)
            
        if needs_extension:
            if not videos_extension:
                raise CommandError("Must specify the extension of your videos.")
            
            if videos_extension[0] != '.':
                raise CommandError("The video extension must start with a dot (.), like .ogv .")
        
        try:
            mode = int(options.get("mode"))
        except:
            raise CommandError("%s is not a valid number." % options.get("mode"))
        
        if match:
            try:
                interpolation = int(options.get("interpolate"))
                assert interpolation >= 0
            except:
                raise CommandError("Interpolation needs to be an integer bigger or equal to 0.")
        
        if shortest_path:
            try:
                source_id = int(options.get("source"))
                target_id = int(options.get("target"))
                assert source_id >= 0
                assert target_id >= 0 
            except:
                raise CommandError("One of %s, %s is not a valid if for a MapPoint." % (options.get("source"), options.get("target")))
        #=======================================================================
        # Execute action
        #=======================================================================
        if match:
            if os.path.isfile(traces_path):
                gpx_paths = [traces_path]
                if os.path.splitext(traces_path)[1] != ".gpx":
                    raise CommandError("%s is not a .gpx file." % traces_path)
            elif os.path.isdir(traces_path):
                gpx_paths = glob.glob("%s/*.gpx" % traces_path)
                if len(gpx_paths) == 0:
                    raise CommandError("No .gpx files in %s." % traces_path)

            for gpx_path in gpx_paths:
                map_match_file(gpx_path, mode, interpolation_factor = interpolation)
                
        if insert:
            geocode_paths = self._require_geocode_paths(traces_path, match)
            insert_geocodes(geocode_paths, videos_path, mode, videos_extension)
        
        if shortest_path:
            self._print("Shortest traces_path from %d to %d" % (source_id, target_id),
                        calculate_shortest_path(source_id, target_id, mode))
        
        if print_ids:
            geocode_paths = self._require_geocode_paths(traces_path, match)
            
            for geocode_path in geocode_paths:
                unique_ids = get_ids(geocode_path)
                self._print("Unique node ids of %s" % os.path.splitext(geocode_path)[0],
                            "\n".join([str(unique_id) for unique_id in unique_ids]))
            
                         
        if draw:
            draw_map(mode)

        return None
    
    def _require_geocode_paths(self, path, match):
        if os.path.isfile(path):
            # The input file was converted to .geocode. Select it.
            if match:
                geocode_paths = [os.path.splitext(path)[0] + ".geocode"]
            else:
                geocode_paths = [path]
                if os.path.splitext(path)[1] != ".geocode":
                    raise CommandError("%s is not a .geocode file." % path)
        elif os.path.isdir(path):
            geocode_paths = glob.glob("%s/*.geocode" % path)
            if len(geocode_paths) == 0:
                raise CommandError("No .geocode files in %s." % path)
        return geocode_paths
    
    def _print(self, title, message):
        delimiter = "#" + "=" * 30
        lines = message.split("\n")
        print delimiter
        print "# " + title
        for line in lines:
            print line
        print delimiter
    
        
