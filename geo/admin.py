'''
Register all model definition for the admin app.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''
from django.contrib import admin
from django.conf import settings

from geo import models

if settings.DEBUG:
    admin.site.register(models.Video)
    admin.site.register(models.MapPoint)
    admin.site.register(models.MapPointConnection)
    admin.site.register(models.TracePoint)
    admin.site.register(models.TracePointConnection)
