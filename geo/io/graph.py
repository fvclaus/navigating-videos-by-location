'''
Plot graph as image.
This module requires matplotlib for its drawing funcationality.
There is a can_draw() function to test the dependencies.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import os
import tempfile
from decimal import Decimal
import types
import logging

from geo.models import TracePointConnection
from geo.models import MapPoint

NODE_COLOR = "#E9D5C1"
TRACE_EDGE_COLOR = "#FFDDDD"
MAP_EDGE_COLOR = "#99FF99"
logger = logging.getLogger(__name__)

def can_draw():
    """Checks if all necessary dependencies are satisfied. 
    Drawing requires some native modules and might therefore be hard to setup."""
    try:
        import matplotlib.pyplot
        import networkx
        return True
    except:
        return False

def draw(path, mappoints, connections, reset = True, draw_tracepoints = True):
    """Plots MapPoints and Connection as a Graph.
    Path should be without a file extension, because _map.png and _trace.png will be added automatically.
    Use reset to start a new matplotlib figure, otherwise the current matplotlib figure will be used.
    This also means that the MapPoints - and Connection will show up in the TracePoint figure.
    Use draw_tracepoints to draw the TracePoints into the same or new figure."""
    try:
        os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
        import matplotlib.pyplot as plt
        import networkx as nx
    except ImportError:
        logger.info("Could not import matplotlib. Skipping visual graph generation")
        return

    mapconnection_graph = nx.DiGraph()
    traceconnection_graph = nx.DiGraph()

    pos = {}
    node_colors = []
    map_edge_colors = []
    trace_edge_colors = []
    edge_colors = []
    
    # If looking at a geographical small area, the absolute coordinates values are very close to each other.
    # Usually in the range of 3-4 decimal places. By extracting the number of common digits,
    # and cutting them away from each point. The points get artificially spaced out without distortion.
    common_places = _count_common_digits(mappoints)

    for point in mappoints:
        name = point.get_label()
        coord = _cut_point_digits(point, common_places)
        pos[name] = coord
        node_colors.append(NODE_COLOR)
        mapconnection_graph.add_node(name)
        traceconnection_graph.add_node(name)


        for connection in connections:
            graph = None
            if isinstance(connection, TracePointConnection):
                trace_edge_colors.append(TRACE_EDGE_COLOR)
                graph = traceconnection_graph
                edge_colors.append(TRACE_EDGE_COLOR)

            else:
                graph = mapconnection_graph
                map_edge_colors.append(MAP_EDGE_COLOR)
                edge_colors.append(MAP_EDGE_COLOR)

            graph.add_edge(connection.map_source.get_label(), connection.map_target.get_label(), cost = connection.cost)


    if reset:
        figure = plt.figure(figsize = (20, 20))
    else:
        figure = plt.gcf();
        figure.set_size_inches(20, 20)
        plt.figure(figure.number)

    nx.draw_networkx(mapconnection_graph, pos, False, node_color = node_colors, edge_color = MAP_EDGE_COLOR)
    nx.draw_networkx_edge_labels(mapconnection_graph, pos, font_size = 5)

    plt.savefig(path + "_map.png")

    if draw_tracepoints :
        if reset:
            figure = plt.figure(figsize = (20, 20))

        mapconnection_graph = None
        nx.draw_networkx(traceconnection_graph, pos, False, node_color = node_colors)
        nx.draw_networkx_edge_labels(traceconnection_graph, pos, font_size = 5)

        plt.savefig(path + "_trace.png")
            
def _count_common_digits(mappoints):
    """Count the number of digits that are the same for all points."""
    n_common_digits = Decimal("Infinity")
    
    for point in mappoints:
        (lat, lon) = point.as_tuple()
        for other_point in mappoints:
            if other_point == point:
                continue
            (olat, olon) = other_point.as_tuple()
            digits_lat = _compare_digits(lat, olat)
            digits_lon = _compare_digits(lon, olon)
            
            if digits_lat < n_common_digits:
                n_common_digits = digits_lat
            if digits_lon < n_common_digits:
                n_common_digits = digits_lon
                
    return n_common_digits
            
def _compare_digits(x, y):
    """Compare two decimals x and y and return the number of common digits."""
    assert isinstance(x, Decimal)
    assert isinstance(y, Decimal)
    
    # Create two arrays that hold all the digits of both decimal values.
    digits_x = x.as_tuple().digits
    digits_y = y.as_tuple().digits
    
    # Return the index of the first different digit.
    for digit_index in range(min(len(digits_x), len(digits_y))):
        if digits_x[digit_index] != digits_y[digit_index]:
            return digit_index
    
    return len(digits_x)

def _cut_point_digits(point, n_places):
    """Cut the coordinate of the point by n_places and return a list of floats."""
    assert isinstance(point, MapPoint)
    assert isinstance(n_places, types.IntType)
        
    (lat, lon) = point.as_tuple()
    return [float(_cut_digits(lat, n_places)),
            float(_cut_digits(lon, n_places))]


def _cut_digits(x, n):
    """Cut n digits from decimal x starting at the front."""
    n_digits = len(x.as_tuple().digits)
    # It is not possible to remove more places than the decimal has.
    assert not n > n_digits
    
    digits = x.as_tuple().digits[n:]
    n_remaining_digits = len(digits)
    # Assure that there are as many trailing digits as possible
    if n_remaining_digits >= 4: exp = -2
    elif n_remaining_digits == 3: exp = -1
    else: exp = 0
    
    return Decimal((0, digits, exp))
                
