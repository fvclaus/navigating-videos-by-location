'''
Converts videos to Ogg Theora.
This is just a wrapper for the command line tool ffmpeg2theora.
Currently, this only works on linux.
ffmpeg2theora can be installed from the repositories.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import subprocess
import tempfile
import os
import platform

from geo.base.exceptions import OggException

class Profile:
    PRO = "pro"
    VIDEOBIN = "videobin"
    PREVIEW = "preview"

class Ogg():

    EXT = ".ogv"
    
    @classmethod
    def can_convert(cls):
        """Tests if necessary system requirements are present."""
        if platform.system() != "Linux":
            return False
        else:
            (location, something_else) = subprocess.Popen("which ffmpeg2theora", 
                                                          stdout = subprocess.PIPE, 
                                                          shell = True).communicate()
            return len(location) > 0
    
    def convert(self, source, target_path):
        """Converts the source file to an ogv video with the filename and directory from target_path.  
        Returns the new path (with the new file extension)."""
        if not self.can_convert():
            raise OggException, "Can convert videos on this system. Refer to the documentation for details."
        
        target_path = self._normalize_path(target_path)
        (temp, temppath) = self._write_to_temp(source)
        args = self._build_args(temppath, target_path)
        process = subprocess.Popen(args, shell=True)
        process.wait()
        exit_code = process.poll()
        os.remove(temppath)
        
        if exit_code != 0:
            raise OggException, "Conversion returned with non-zero exit code."
        
        return target_path

    def _build_args(self, sourcepath, targetpath):
        return " ".join(["ffmpeg2theora", "-p %s" % Profile.VIDEOBIN, "-o %s" % targetpath, sourcepath])

    def _normalize_path(self, path):
        """Add the proper file extension to the path."""
        return os.path.join(
                            os.path.abspath(path),
                            os.path.splitext(path)[0] + self.EXT
                            )

    def _write_to_temp(self, source):
        ext = os.path.splitext(source.name)[1]
        path = tempfile.mktemp() + ext
        temp = open(path, "wb")
        try:
            byte = source.read(1)
            while byte != "":
                temp.write(byte)
                byte = source.read(1)
        finally:
            temp.close()
            source.close()

        return (temp, path)