'''
Contains class definition to read the gpx format.
Code based on http://the.taoofmac.com/space/blog/2005/10/11/2359

GPX defines one or more tracks that are separated in track segments.
Currently, all track segments of the first track will be read in order.
Any other tracks after the first one will be ignored.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import re, logging
from time import strptime, mktime
from decimal import Decimal
from datetime import datetime
from xml.dom import minidom

from geo.base.exceptions import NoTrkPtException, NoTrkSegTagException, NoTrkTagException


class GPX():
    "GPX Parser for file-like objects."

    DATETIME_FMT = "%Y-%m-%d %H:%M:%S"
    logger = logging.getLogger(__name__)

    def __init__(self, input_file = None, no_time = False):
        """Use no_time if the file should not be checked for a timestamp."""
        self.file = input_file
        self.no_time = no_time
        self.parsed = False
        

    def is_valid(self):
        """Checks if the current gpx file is valid.
        A gpx file is valid if it has at least one track and one track segment.
        All track points must have a geographic coordinate and a time (if no_time is false)."""
        try:
            self._parse()
            for (lat, lon, time) in self.get_next_track_point():
                if (float(lat) == None) or (float(lon) == None) or (not self.no_time and time == None):
                    return False
        except:
            return False

        return True

    def get_track_points(self):
        """Returns all track_points as list.
        A track point is a triple (lat, lon, time)."""
        trackpoints = []
        for data in self.get_next_track_point():
            trackpoints.append(data)
        return trackpoints

    def get_next_track_point(self):
        """Get the next track point. Use in for loop.
        A trackpoint is a triple (lat, lon, time)."""
        self._parse()
        time_pattern = re.compile("(?P<date>\d{4}-\d{2}-\d{2})T(?P<time>\d{2}:\d{2}:\d{2})")
        trksegs = self.track.getElementsByTagName('trkseg')

        if not trksegs:
            raise NoTrkSegTagException , "document does not has a track segment tag"

        for trkseg in trksegs:
            trkpts = trkseg.getElementsByTagName('trkpt')

            if not trkpts:
                raise NoTrkPtException, "document does not has a track point tag"

            for trkpt in trkpts:
                lat = Decimal(trkpt.getAttribute('lat'))
                lon = Decimal(trkpt.getAttribute('lon'))
                try:
                    time = trkpt.getElementsByTagName('time')[0].firstChild.data
                    match = time_pattern.match(time)
                    time = " ".join(match.groups())
                    time = strptime(time, self.DATETIME_FMT)
                    time = datetime.fromtimestamp(mktime(time))
                except Exception, e :
                    if self.no_time:
                        time = None
                    else:
                        raise e
                yield (lat, lon, time)

    def set_track_points(self, mapconnections_dto):
        """Possible input is graph.as_unique_mapconnections().
        Use this to render a graph as .gpx."""
        xml = minidom.Document()
        gpx = xml.createElement("gpx")
        gpx.setAttribute("version", "1.1")
        xml.appendChild(gpx)
        trk = xml.createElement("trk")
        gpx.appendChild(trk)
        for mappointtrack in mapconnections_dto:
            trkseq = xml.createElement("trkseg")
            trk.appendChild(trkseq)
            for mappoint in mappointtrack:
                trkpt = xml.createElement("trkpt")
                trkpt.setAttribute("lat", str(mappoint.lat))
                trkpt.setAttribute("lon", str(mappoint.lon))
                trkseq.appendChild(trkpt)
        self.xml = xml
        
    def as_string(self):
        return self.xml.toprettyxml()
        
    def _parse(self):
        if self.parsed:
            return 
        
        self.parsed = True
        try:
            doc = minidom.parse(self.file)
            doc.normalize()
        except Exception as e:
            raise e
        gpx = doc.documentElement
        tracks = gpx.getElementsByTagName('trk')
        if len(tracks) < 1:
            raise NoTrkTagException, "document does not has a track tag"
        self.track = tracks[0]