'''
Defines graph data structure for OSM data.
This is used as input to the map-matching process.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import sys
from decimal import Decimal

from geo.base.vector import normalize_vector, subtract_vectors, dot_product, calculate_distance
from geo.base.exceptions import OSMException

class OSMGraph():
    """Root of the OSM graph.
    It defines matching heuristics that select the best match for a coordinate on a given set of links."""
    def __init__(self, nodes, ways, bidirectional = False):
        """Use bidirectional if the ways are only defined one way but can be traversed in both ways."""
        self.nodes = {}
        for node in nodes:
            osmnode = OSMNode(node)
            self.nodes[osmnode.id] = osmnode

        self.links = []
        for way in ways:
            # Get the nodes from this link.
            nds = way.getElementsByTagName("nd")
            self._add_way(nds)
            #TODO There is a special attribute on way that marks a way as one-way street.
            # Read the attribute and if not present, reverse the way additionally.
            # Note: Street trains do not define this attribute, but really only go one way.
            # The name of the one-way attribute can be found in OSM documentation.
            if bidirectional:
                nds.reverse()
                self._add_way(nds)
                
    def _add_way(self, nds, relabel = False):
        predecessor = None
        for nd in nds:
            node_id = nd.getAttribute("ref")
            node = self.nodes[node_id]
            # Not the first node of the link
            if predecessor != None:
                # Create a new link and link and add it to the predecessor (nodes only store outgoing links)
                link = OSMLink(predecessor, node)
                self.links.append(link)
                predecessor.add_link(link)
                
            predecessor = node
            
    def graph_match(self, coordinate, direction):
        """Match the coordinate on the whole graph without any context."""
        links = self.links
        return self._match(coordinate, direction, links)
        
                    
    def subgraph_match(self, coordinate, direction, links):
        """Match the coordinate topologically."""
        return self._match(coordinate, direction, links)
                           
    def _match(self, coordinate, direction, links):
        max_weight = -sys.maxint
        best_link = None
        distances = []
        coordinate_direction = normalize_vector(direction)
        
        for link in links:
            # Calculate the TWS. See the module documentation for more information.
            distance = calculate_distance(coordinate, link.source)
            heading = dot_product(link.normalized_direction, coordinate_direction)
            # Make distances more important than the heading.
            # Oftentimes the heading is off when accelerating.
            # Note: In the original paper, the heading has been calculated with a gyroscope.
            weight = 3 * ((20 - distance * 1000) / 20) + float(2 * heading)

            if weight > max_weight:
                best_link = link   
                max_weight = weight 
                
            distances.append(distance)
                
        
        if best_link is None:
            # TODO Can really be all links on the blacklist.
            raise OSMException, "Could not find a nearby node!"  
        
        return (best_link.source, best_link, distances)
    
 

class OSMNode():
    
    def __init__(self, node, relabel = False):
        # Relabeling is needed when creating new nodes to reverse existing links that only go in one direction.
        if relabel:
            self.id = "%s_RELABEL" % node.getAttribute("id")
        else:
            self.id = node.getAttribute("id")
        self.lat = Decimal(node.getAttribute("lat"))
        self.lon = Decimal(node.getAttribute("lon"))
        self.links = []
        
    def add_link(self, link):
        self.links.append(link)
        
    @property
    def successors(self):
        return [link.target for link in self.links]
    
    def is_adjacent(self, node):
        return node in [link.target for link in self.links]
    
    def __eq__(self, other):
        if isinstance(other, OSMNode):
            return self.id == other.id
        return False
    
    def __getitem__(self, x):
        if x == 0: 
            return self.lat
        elif x == 1:
            return self.lon
        else:
            raise IndexError, "Index %s is not a valid index." % x
          
    def __hash__(self):
        return int(self.id)
    
    def __str__(self):
        return self.id
    # Useful for debugging in eclipse.
    def __repr__(self):
        return self.id
    
class OSMLink():
    
    def __init__(self, source, target):
        self.source = source
        self.target = target
        self.normalized_direction = normalize_vector(subtract_vectors(self.source, self.target))
    
    def __str__(self):
        return "%s -> %s" % (self.source, self.target)
    # Useful for debugging in eclipse.
    def __repr__(self):
        return "%s -> %s" % (self.source, self.target)
