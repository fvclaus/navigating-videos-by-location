'''
Tests the ogg module, only if the system requirements are met.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

from unittest import TestCase
from settings import TEST_PATH
import os

from geo.io.ogg import Ogg


if Ogg.can_convert():
    class OggTest(TestCase):
                
        def test_convert(self):
            source = os.path.join(TEST_PATH, "video.3gp")
            target = os.path.join(TEST_PATH, "video_out.ogv")
            ogg = Ogg()
            print "Start conversion of %s" % (source,)
            source = open(source, "rb")
            ogg.convert(source, target)
            self.assertTrue(os.path.exists(target), "Converted video should exist")
            os.remove(target)