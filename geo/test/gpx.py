'''
Test module for GPX.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

from unittest import TestCase
import os
from decimal import Decimal

from django.conf import settings

from geo.base.exceptions import NoTrkPtException, NoTrkSegTagException, NoTrkTagException
from geo.io.gpx import GPX


class GPXTest(TestCase):

    VALID_PATH = os.path.join(settings.TEST_PATH, "valid.gpx")
    INVALID_TIME_PATH = os.path.join(settings.TEST_PATH, "invalid_time.gpx")
    INVALID_COORD_PATH = os.path.join(settings.TEST_PATH, "invalid_coord.gpx")
    NO_TRKPT_PATH = os.path.join(settings.TEST_PATH, "no_trkpt.gpx")
    NO_TRKSEQ_PATH = os.path.join(settings.TEST_PATH, "no_trkseq.gpx")
    NO_TRK_PATH = os.path.join(settings.TEST_PATH, "no_trk.gpx")

    VALID_TRACKPOINTS = [(Decimal("49.48483586311197"), Decimal("8.463313579559081"), "2012-03-07 11:38:07"),
                        (Decimal("49.484910964964385"), Decimal("8.463399410247558"), "2012-03-07 11:38:12"),
                        (Decimal("49.4849860668168"), Decimal("8.463485240936034"), "2012-03-07 11:38:39"),
                        (Decimal("49.48500752448892"), Decimal("8.463656902312987"), "2012-03-07 11:38:45"),
                        (Decimal("49.48499679565286"), Decimal("8.463742733001464"), "2012-03-07 11:38:49"),
                        (Decimal("49.48510408401346"), Decimal("8.463721275329345"), "2012-03-07 11:39:12"),
                        (Decimal("49.48521137237405"), Decimal("8.463699817657226"), "2012-03-07T11:39:16")]
    
    
    def setUp(self):
        self.valid = GPX(open(self.VALID_PATH, "r"))
        self.invalidcoord = GPX(open(self.INVALID_COORD_PATH, "r"))
        self.invalidtime = GPX(open(self.INVALID_TIME_PATH, "r"))
        self.notrk = GPX(open(self.NO_TRK_PATH, "r"))
        self.notrkpt = GPX(open(self.NO_TRKPT_PATH, "r"))
        self.notrkseq = GPX(open(self.NO_TRKSEQ_PATH, "r"))

    def test_is_valid(self):
        self.assertTrue(self.valid.is_valid(), "Valid gpx file should validate to true")
        invalid_msg = "Invalid gpx file should validate to false"
        self.assertFalse(self.invalidcoord.is_valid(), invalid_msg)
        self.assertFalse(self.invalidtime.is_valid(), invalid_msg)
        self.assertFalse(self.notrk.is_valid(), invalid_msg)
        self.assertFalse(self.notrkpt.is_valid(), invalid_msg)
        self.assertFalse(self.notrkseq.is_valid(), invalid_msg)

    def test_get_trackpoints_invalid(self):
        self.assertRaises(NoTrkTagException, self.notrk.get_track_points)
        self.assertRaises(NoTrkSegTagException, self.notrkseq.get_track_points)
        self.assertRaises(NoTrkPtException, self.notrkpt.get_track_points)
        
    def test_get_trackpoints_valid(self):
        trackpoints = self.valid.get_track_points()
        for index in range(len(trackpoints)):
            validtrackpoint = self.VALID_TRACKPOINTS[index]
            trackpoint = trackpoints[index]

            self.assertEqual(validtrackpoint[0], trackpoint[0], "lat should be equal")
            self.assertEqual(validtrackpoint[0], trackpoint[0], "lng should be equal")
            self.assertEqual(validtrackpoint[0], trackpoint[0], "date should be equal")
