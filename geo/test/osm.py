# coding=UTF-8
'''
Tests the map-matching with some real-world .gpx files.
For every .gpx file, their is a .nodes file containing a sequence of OSM nodes.
The sequence of nodes represents the correct matching and was cross-referenced with JOSM.

There is also the switch --print-ids in the command utility that will generate the same syntax used in .nodes files.

Custom map-matching parameter need to be specified in a .config file using JSON syntax.
E.g. { "FromAtoB.gpx" : { "interpolate" : 3 }} 


@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import glob
import os
import logging
import json

from django.test import TestCase
from django.conf import settings

from geo.core.mapmatching import OSM
from geo.io.gpx import GPX


class OSMTest(TestCase):
    
    logger = logging.getLogger(__name__)
        
        
    def test_map_match(self):
        geocoder = OSM(0)
        config_file = open(os.path.join(settings.TRACKS_DIR, "config.json"), "r")
        config = json.loads(config_file.read())
        # Sort alphabetically to ensure the same order for each run
        nodes_paths = glob.glob("%s/*.nodes" % settings.TRACKS_DIR)
        nodes_paths.sort()
        for nodes_path in nodes_paths:
            self.nodes_path = nodes_path
            nodes_root = os.path.splitext(nodes_path)[0]
            nodes_config = config.get(os.path.basename(nodes_root), {})
            gpx = GPX(open(nodes_root + ".gpx", "r"))
            coordinates = gpx.get_track_points()
            interpolation_factor = nodes_config.get("interpolate", 0)
            self.logger.info("Using interpolation %d." % interpolation_factor)
            geocodes = geocoder.match(coordinates, interpolation_factor)
            geocodes_node_ids = [int(geocode[3]) for geocode in geocodes]
            actual_node_ids = []
            
            for node in geocodes_node_ids:
                if len(actual_node_ids) == 0 or not node == actual_node_ids[-1]:
                    actual_node_ids.append(node)
                    
            expected_node_ids = self.read_node_ids(nodes_path)
            
            
            
            for node in expected_node_ids:
                if node not in actual_node_ids or actual_node_ids.index(node) != expected_node_ids.index(node):
                    self.logger.error("Node %d not found or index not equal. \n" \
                                      "Expected: %s.\nActual: %s.\n" \
                                      "The difference between the expected and actual node set is is %s." % (node,
                                                                                                           expected_node_ids,
                                                                                                           actual_node_ids,
                                                                                                           set(actual_node_ids).symmetric_difference(expected_node_ids)))
                    
                self.assertTrue(node in actual_node_ids)
                self.assertEqual(actual_node_ids.index(node), expected_node_ids.index(node))
                
            if len(expected_node_ids) > len(actual_node_ids):
                self.logger.error("These nodes are missing: %s" % (set(expected_node_ids).difference(set(actual_node_ids))))
            elif len(expected_node_ids) < len(actual_node_ids):
                self.logger.error("These nodes were mapped but not expected: %s" % (set(actual_node_ids).difference(expected_node_ids)))
                
            self.assertEqual(len(expected_node_ids), len(actual_node_ids))            
    
    def read_node_ids(self, nodes_path):
        node_ids = []
        nodes_file = open(nodes_path, "r")
        for node in nodes_file:
            node_ids.append(int(node))
        return node_ids
    
    def tearDown(self):
        # This is called when one test fails.
        # Log the last file for debugging.
        self.logger.info("Last nodes file was: %s" % self.nodes_path)
