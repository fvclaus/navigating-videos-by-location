'''
Created on Mar 23, 2012

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

from StringIO import StringIO
from copy import deepcopy
import os
from decimal import Decimal
from datetime import datetime, timedelta
import tempfile

from django.test.testcases import TransactionTestCase
from django.conf import settings
from django.db import transaction

from geo.io.gpx import GPX
from geo.core.routing import Graph
from geo.base.util import TracePointDTO
from geo.models import createvideo, MapPointConnection


#===============================================================================
# Create test data and convert it to traces of sequences of TracePointDTO instances.
#===============================================================================
NOW = datetime.now()
# Holds the trace data as TracePointDTO instance.
DTO_TRACES = []
VIDEO_PATH = os.path.join(settings.TEST_PATH, "video.3gp")

def createtime(**kwargs):
    return NOW - timedelta(**kwargs)

TRACES = [#=====================================================================
          # Forward in a lightning bolt pattern.
          #=====================================================================
          [(Decimal("1.0"), Decimal("0.5"), createtime(minutes = 20), 0),
           (Decimal("1.001"), Decimal("0.5"), createtime(minutes = 19), 60),
           (Decimal("1.002"), Decimal("0.5"), createtime(minutes = 18), 120)],
          
          [(Decimal("1.002"), Decimal("0.5"), createtime(minutes = 13), 0),
           (Decimal("1.002"), Decimal("0.501"), createtime(minutes = 12), 60),
           (Decimal("1.002"), Decimal("0.502"), createtime(minutes = 11), 120)],
          
          [(Decimal("1.002"), Decimal("0.502"), createtime(minutes = 6), 0),
           (Decimal("1.003"), Decimal("0.502"), createtime(minutes = 5), 60),
           (Decimal("1.004"), Decimal("0.502"), createtime(minutes = 4), 120)],
          #=====================================================================
          # Backwards in the same lightning bolt.
          #=====================================================================
          [(Decimal("1.004"), Decimal("0.502"), createtime(minutes = 18), 0),
           (Decimal("1.003"), Decimal("0.502"), createtime(minutes = 19), 60),
           (Decimal("1.002"), Decimal("0.502"), createtime(minutes = 20), 120)],
          
          [(Decimal("1.002"), Decimal("0.502"), createtime(minutes = 13), 0),
           (Decimal("1.002"), Decimal("0.501"), createtime(minutes = 12), 60),
           (Decimal("1.002"), Decimal("0.5"), createtime(minutes = 11), 120)],
          
          [(Decimal("1.002"), Decimal("0.5"), createtime(minutes = 6), 0),
           (Decimal("1.001"), Decimal("0.5"), createtime(minutes = 5), 60),
           (Decimal("1.0"), Decimal("0.5"), createtime(minutes = 4), 120)],
          #=====================================================================
          # Somewhere else
          #=====================================================================
          [(Decimal("3.002"), Decimal("0.5"), createtime(minutes = 3), 0),
           (Decimal("3.001"), Decimal("0.5"), createtime(minutes = 2), 60),
           (Decimal("3.0"), Decimal("0.5"), createtime(minutes = 1), 120)]]

# Build the DTO_TRACES
for trace in TRACES:
    dtos = []
    for point in trace:
        dtos.append(TracePointDTO(lat = point[0], lon = point[1], real_time = point[2], video_time = point[3]))
    DTO_TRACES.append(dtos)
    
    
class GraphTest(TransactionTestCase):
    
    def setUp(self):
        """Database access is only available once the tests are running.
        Therefore the videos can only be created here."""
        self.videos = []
        for trace in DTO_TRACES:
            # Create a new video for every trace. Otherwise, Graph will aggregate everything into one trace.
            video = createvideo(VIDEO_PATH)
            self.videos.append(video)
            for dto in trace:
                dto.video = video

    #===========================================================================
    # MapPoint
    #===========================================================================
    @transaction.commit_on_success
    def test_insert_mappoints(self):
        "Insert MapPoints and check if they persist."
        graph = Graph(2)
        for index in range(len(TRACES)):
            self._insert_mappoints(TRACES[index], graph)
            
    def _insert_mappoints(self, trace, graph):
        """Insert MapPoint and test the get_mappoint function."""
        for (lat, lon, real_time, video_time) in trace:
            mappoint1 = graph._insert_mappoint(lat, lon)
            mappoint2 = graph.get_mappoint(pk = mappoint1.id)
            mappoint3 = graph.get_mappoint(lat = mappoint1.lat, lon = mappoint1.lon)

            self.assertEqual(mappoint1, mappoint2, "Insert and get should retrieve the same MapPoint.")
            self.assertEqual(mappoint2, mappoint3, "Get with id and get with lat|lon should retrieve the same MapPoint.")
            
    #===========================================================================
    # TracePoint
    #===========================================================================
    @transaction.commit_on_success
    def test_insert_tracepoints(self):
        """Insert all TracePoints and check MapPoint creation and video times."""
        graph = Graph(2)
        for index in range(len(DTO_TRACES)):
            self._insert_tracepoints(DTO_TRACES[index], self.videos[index], graph)
            
    @transaction.commit_on_success
    def test_reverse_insert_tracepoints(self):
        """Insert all TracePoints in reverse order and check MapPoint creation and video times."""
        graph = Graph(2)
        for index in range(len(DTO_TRACES)):
            trace = deepcopy(DTO_TRACES[index])
            trace.reverse()
            self._insert_tracepoints(trace, self.videos[index], graph)
            
    def _insert_tracepoints(self, dtos, video, graph):
        """Inserts new TracePoints and checks if MapPoints are created accordingly.
        Also checks if the video and real times are set correctly."""
        oldmappoint = None
        for dto in dtos:
            tracepoint = graph._insert_tracepoint(dto)
            mappoint = graph.get_mappoint(lat = dto.lat, lon = dto.lon)
            self.assertTrue(mappoint, "_insert_tracepoint should have created a new mappoint")
            self.assertEqual(graph.get_tracepoint(video = video, point = mappoint), tracepoint)
            
            # Check if the video_time is set correctly.
            if oldmappoint != None and oldmappoint == mappoint:
                self.assertEqual(tracepoint.real_time_end, dto.real_time, "TracePoint should have ending time equal to most recent one.")
                self.assertEqual(tracepoint.video_time_end, dto.video_time, "TracePoint should have video end time equal to the most recent one.")
            else:
                self.assertEqual(tracepoint.real_time_start, dto.real_time, "TracePoint should have starting time equal to the first one.")
                self.assertEqual(tracepoint.real_time_end, dto.real_time, "TracePoint should have ending time equal to the first one.")
                self.assertEqual(tracepoint.video_time_end, dto.video_time, "TracePoint should have video end time equal to the first one.")
                self.assertEqual(tracepoint.video_time_start, dto.video_time, "TracePoint should have video start time equal to the first one.")

            oldmappoint = mappoint
            
    #===============================================================================
    # TracePointConnection 
    #===============================================================================
    @transaction.commit_on_success
    def test_insert_traceconnections(self):
        """Inserts TracePoinConnections and that the Connection between two TracePoints is not duplicated."""
        graph = Graph(2)
        for index in range(len(DTO_TRACES)):
            self._insert_traceconnections(DTO_TRACES[index], self.videos[index], graph)
        # There is a MapPointConnection between every two points that are connected with one ore more TracePointConnections.
        self.assertTrue(MapPointConnection.objects.all().count() == len(DTO_TRACES) * 2, "There is no need for a MapPointConnection.")

    def _insert_traceconnections(self, trace, video, graph):
        oldtracepoint = None
        graph.insert_trace(trace)
        
        for dto in trace:
            tracepoint = graph.get_tracepoint(video, lat = dto.lat, lon = dto.lon)

            if oldtracepoint != None and oldtracepoint != tracepoint:
                traceconnection = graph._get_traceconnection(trace_source = oldtracepoint, trace_target = tracepoint)
                self.assertTrue(traceconnection, "TracePointConnection should have been created.")
                traceconnections = graph._get_traceconnections(map_source = oldtracepoint.mappoint, map_target = tracepoint.mappoint, video = video)
                self.assertEqual(len(traceconnections), 1, "There should be exactly one TracePointConnection.")
            oldtracepoint = tracepoint

    #===========================================================================
    # Export
    #===========================================================================
    def test_as_unique_mapconnections(self):
        """Checks that MapConnections are exported properly."""
        graph = Graph(2)
        self._insert_all(graph)
        
        graph_dto = graph.as_unique_mapconnections()
        gpx = GPX()
        gpx.set_track_points(graph_dto)
        xml = StringIO(gpx.xml.toprettyxml())
        gpx = GPX(xml, no_time = True)
        
        trackpoints = gpx.get_track_points()
        
        for trace_index in range(len(DTO_TRACES)):
            trace = DTO_TRACES[trace_index]
            for dto_index in range(len(trace)):
                dto = TRACES[trace_index][dto_index]
                self.assertTrue((dto[0], dto[1], None) in trackpoints)
                
    def test_draw(self):
        graph = Graph(2)
        self._insert_all(graph)
        
        if graph.can_draw():
            path = tempfile.mktemp()
            graph.draw(path)
            map_path = path + "_map.png"
            trace_path = path + "_trace.png"
            self.assertTrue(os.path.exists(map_path))
            self.assertTrue(os.path.exists(trace_path))
            os.remove(map_path)
            os.remove(trace_path)
            graph.draw(path, draw_tracepoints = False)
            self.assertFalse(os.path.exists(trace_path))
            os.remove(map_path)

    #===========================================================================
    # Shortest path
    #===========================================================================
    def test_shortest_path_simple(self):
        graph = Graph(2)
        self._insert_all(graph)
       
        sourcedto = DTO_TRACES[0][0]
        targetdto = DTO_TRACES[0][-1]
        source = graph.get_mappoint(lat = sourcedto.lat, lon = sourcedto.lon)
        target = graph.get_mappoint(lat = targetdto.lat, lon = targetdto.lon)
        shortest_path = graph.calculate_shortest_path(source, target)
        expected = "1>(1)2>(1)3"
        self.assertEqual(expected, str(shortest_path))
        
    

    def test_shortest_path_hard(self):
        graph = Graph(2)
        self._insert_all(graph)
        
        # TODO the drawing does not work anymore
        # probably because the nodes label is different than the one from the position array
        # this indicates problems with decimal precision again
#        graph.draw("/tmp/graph")
        
        # Forward
        source_dto = DTO_TRACES[0][0]
        target_dto = DTO_TRACES[2][-1]
        shortest_path = self._calculate_shortest_path(source_dto, target_dto, graph)
        expected_path = "1>(1)2>(1)3>(2)4>(2)5>(3)6>(3)7"
        self.assertEqual(str(shortest_path), expected_path)
        
        # Backward
        source_dto = DTO_TRACES[3][0]
        target_dto = DTO_TRACES[5][-1]
        shortest_path = self._calculate_shortest_path(source_dto, target_dto, graph)
        expected_path = "7>(4)6>(4)5>(5)4>(5)3>(6)2>(6)1"
        self.assertEqual(str(shortest_path), expected_path)
        
        # Cannot be reached.
        source_dto = DTO_TRACES[3][0]
        target_dto = DTO_TRACES[6][0]
        shortest_path = self._calculate_shortest_path(source_dto, target_dto, graph)
        self.assertEqual(str(shortest_path), "")
        
    def _calculate_shortest_path(self, source_dto, target_dto, graph):
        source = graph.get_mappoint(lat = source_dto.lat, lon = source_dto.lon)
        target = graph.get_mappoint(lat = target_dto.lat, lon = target_dto.lon)

        shortest_path = graph.calculate_shortest_path(source, target)

#        path = os.path.join(settings.TEST_PATH, "shortest_path_%d_%d.gpx" % (source.id, target.id))
#        gpx = GPX()
#        gpx.pointtrace(pointtrack)
#        gpx.write(open(path, "w"))


        return shortest_path
        
        
    def _insert_all(self, graph):
        graph.insert_traces(DTO_TRACES)
        

    
#
#    pathtocolor = {
#                   GEOCODE1_PATH :"#FF0000",
#                   GEOCODE2_PATH : "#2AFF38",
#                   GEOCODE3_PATH : "#AD4343",
#                   GEOCODE4_PATH : "#275C2B",
#                   GEOCODE5_PATH : "#EBB1B1",
#                   GEOCODE6_PATH : "#81D287" }
#
#    def testgeneratealltracesgeocode(self):
#        graph = self.graph
#        config = {"geocode1" : [self.pathtodtos[self.GEOCODE1_PATH], {"node_color" : self.pathtocolor[self.GEOCODE1_PATH]}],
#                  "geocode3" : [self.pathtodtos[self.GEOCODE3_PATH], {"node_color" : self.pathtocolor[self.GEOCODE3_PATH]}],
#                  "geocode5" : [self.pathtodtos[self.GEOCODE5_PATH], {"node_color" : self.pathtocolor[self.GEOCODE5_PATH]}],
#                  "geocode2" : [self.pathtodtos[self.GEOCODE2_PATH], {"node_color" : self.pathtocolor[self.GEOCODE2_PATH]}],
#                  "geocode4" : [self.pathtodtos[self.GEOCODE4_PATH], {"node_color" : self.pathtocolor[self.GEOCODE4_PATH]}],
#                  "geocode6" : [self.pathtodtos[self.GEOCODE6_PATH], {"node_color" : self.pathtocolor[self.GEOCODE6_PATH]}]
#                  }
#        path = os.path.join(settings.TEST_PATH, "all_geocode")
#
#        graph.drawmultiple(config, path)
#
#    def testgeneratealltraces(self):
#        graph = self.graph
#        config = {}
#        key = 1
#        for path in self.pathtodtos.keys():
#            config[key] = [self.getdtos(path), {"node_color" : self.pathtocolor[path]}]
#            key += 1
#        path = os.path.join(settings.TEST_PATH, "all_gpx")
#
#        graph.drawmultiple(config, path)
#
#
#    def getdtos(self, path):
#        (name, ext) = os.path.splitext(path)
#        gpx = GPX(open(name + ".gpx"))
#        video = createvideo(self.VIDEO_DIR)
#        dtos = []
#        for (lat, lon, time) in gpx.gettrackpoint():
#            dto = TracePointDTO(lat, lon, time, 0, video)
#            dtos.append(dto)
#        return dtos
#
#
#    def insert_trace(self):
#        graph = self.graph
#        graph.insert_trace(self.pathtodtos[self.GEOCODE1_PATH])
#        graph.insert_trace(self.pathtodtos[self.GEOCODE3_PATH])
#        graph.insert_trace(self.pathtodtos[self.GEOCODE5_PATH])

