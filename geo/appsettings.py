'''
This is not a django required settings module.
It contains the configuration needed for the autoloader to find the right packages
for the models and tests.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''
import os


APP_PATH = os.path.abspath(os.path.dirname(__file__))
APP_NAME = "geo"
MODEL_DEFINITION = ["routing" ]
TEST_DEFINITION = ["test"]
