# Create your views here.
'''
Defines views to:
- Render the landing page and upload traces + videos
- Return the graph or a shortest path as json

@author Frederik Claus <f.v.claus@googlemail.com>
'''

import os
import tempfile
import logging
from tempfile import mkstemp
import re

from django.views.generic.simple import direct_to_template
from django.http import Http404, HttpResponse, HttpResponseBadRequest



from geo.io.gpx import GPX
from geo.io.ogg import Ogg
from geo.core.routing import Graph 
from geo.forms import TrackForm, UploadForm
from geo.management.commands.videotagging import map_match_file, geocode_to_dto
from geo.base import wiki2plain

from wikitools import Wiki, Page

logger = logging.getLogger(__name__)

def index(request):
    if request.method == "GET":
        return direct_to_template(request, template = "index.html", extra_context = {"form" : UploadForm()})
    else:
        #TODO This neither used nor tested and probably does not work anymore.
        form = UploadForm(request.POST, request.FILES)
        messages = []
        errors = []

        if form.is_valid():
            logger.debug("Upload form for mode %s is valid", form.cleaned_data["transportation_mode"])
            gpx_file = to_temporary_file(request.FILES["gps_trace"])
            videofile = to_temporary_file(request.FILES["video"])
            newvideopath = to_temporary_file().name

            gpx = GPX(gpx_file)

            # Convert video, resolve points and add everything to the graph
            if gpx.is_valid():
                logger.debug("GPX seems to be valid")
                ogg = Ogg()
                ogg_video_path = ogg.convert(videofile, newvideopath)
                logger.debug("video path: %s", ogg_video_path)
                map_match_file(gpx_file.name, suffix = os.path.splitext(gpx_file.name)[1])
                gpx_file.seek(0)
                dtos = geocode_to_dto(gpx_file, ogg_video_path)
                graph = Graph.get_instance(form.cleaned_data["transportation_mode"])
                graph.insert_trace(dtos)
                logger.debug("points added")
                os.remove(ogg_video_path)
                os.remove(gpx_file.name)
                messages.append("Upload successful. The trace has been added to the graph.")
            else:
                logger.debug("gpx not valid")
                errors.append("The supplied .gpx is not a valid GPX trace.")

            return direct_to_template(request, template = "index.html", extra_context = {"form" : form, "messages" : messages, "errors" : errors})



def to_temporary_file(upload = None, mode = "rw"):
    if not upload:
        return open(mkstemp()[1], mode)
    
    logger.debug("Upload temp_file size: %d", upload.size)
    logger.debug("Upload temp_file name: %s", upload.name)
    path = tempfile.mkstemp()[1]

    temp_file = open(path, "w+b")
    for chunk in upload.chunks():
        temp_file.write(chunk)
    logger.debug("created temporary temp_file %s" % (temp_file.name))
    temp_file.seek(0)
    return temp_file



def trace(request):
    if request.method == "POST":
        raise Http404
    trackform = TrackForm(request.GET)
    if not trackform.is_valid():
        return HttpResponseBadRequest(content = failure(trackform.errors), content_type = "text/json")
    logger.info("Request is valid. Get graph %d", trackform.cleaned_data["mode"])
    graph = Graph.get_instance(trackform.cleaned_data["mode"])

    source_id = trackform.cleaned_data["source"]
    target_id = trackform.cleaned_data["target"]

    if source_id and target_id:
        logger.info("Calculate shortest path")
        source = graph.get_mappoint(pk = source_id)
        target = graph.get_mappoint(pk = target_id)
        if not source or not target:
            return HttpResponseBadRequest(content = failure("did not found source or target"), content_type = "text/json")
        shortest_path = graph.calculate_shortest_path(source, target)
        logger.debug(shortest_path)
        return HttpResponse(content = shortest_path.as_json(), content_type = "text/json")

    else:
        logger.info("Calculate whole graph")
        pointtraces = graph.as_unique_mapconnections()
        logger.info("Got %d pointtraces" % len(pointtraces))
        return HttpResponse(content = pointtraces.as_json(), content_type = "text/json")
    

"""
Returns the plain text of wikipedia article with given title
@author Jan Vorcak <janvor@ifi.uio.no>
"""
def wikipedia(request, title, language = "en"):
    if request.is_ajax():
        try: 
            wikipage = Page(Wiki("http://" + language + ".wikipedia.org/w/api.php"), title = title)
            text = wiki2plain.Wiki2Plain(wikipage.getWikiText()).text
            text = re.sub(r"===([^=]*)===", r"<h2>\1</h2>", text)
            text = re.sub(r"==([^=]*)==", r"<h3>\1</h3>", text)
            return HttpResponse(text)
        except:
            return HttpResponse("")
    else:
        return HttpResponse("")
    

def failure(error):
    return {"success" : False,
            "error" : str(error)}

def success():
    return {"success" : True}
