# Create your models here.

import os
import math

from django.db import models
from django.conf import settings
from django.core.files import File

from geo import appsettings
from geo.base.util import ConnectionMode

#===============================================================================
# Video 
#===============================================================================
if settings.IS_APPENGINE:
    class Video(models.Model):
        """Video model for GAE.
        Videos on GAE are static data in the videos folder (aliased as /static/video/).
        This model just stores the relative URL to the video."""
        video = models.CharField(max_length = 200)
        
        @property
        def path(self):
            return self.video

        def __unicode__(self):
            return self.video

    def createvideo(path):
        filename = os.path.split(path)[1]
        video = Video(video = "static/video/%s" % filename)
        video.save()
        return video
else: 
    class Video(models.Model):
        """Video model for non GAE.
        It stores the video data in the upload dir."""
        video = models.FileField(upload_to = settings.UPLOAD_DIR)

        @property
        def path(self):
            path = self.video.path.replace(settings.PROJECT_PATH, "")
            if path[0] == os.sep:
                path = path[1:]
            return path

        def __unicode__(self):
            return self.video.name

    def createvideo(path):
        video = Video(video = File(open(path, "rb")))
        video.save()
        return video


#===============================================================================
# Abstract super class
#===============================================================================

class Point(models.Model):
    """ Abstract base class for all Points. It defines a Meta class that stores the name of the app."""
    # Radius of what is considered near in different transportation modes.
    # The radius is set in meters.
    IS_NEAR = ((ConnectionMode.WALK, 30),
               (ConnectionMode.BIKE, 60),
               (ConnectionMode.MOTOR_VEHICLE, 200),
               (ConnectionMode.TRAIN, 1000))
    mode = models.IntegerField()

    class Meta:
        # If the app_label is not set, django will not be able to map a model definition to a specific app.
        # As a consequence, django will not load definitions outside of models.py without an app label.
        app_label = appsettings.APP_NAME
        abstract = True
        
#===============================================================================
# Implementation
#===============================================================================

class MapPoint(Point):
    """Point from a GIS.
    A MapPoint is wrapper for a GIS node (atm: OSM).
    All MapPoints form the nodes of the graph that is covered by current video data.
    Every MapPoint is unique in it's coordinates.
    """
    # Creates decimals with 2 leading and 7 trailing decimal places.
    lat = models.DecimalField(decimal_places = 7, max_digits = 9)
    lon = models.DecimalField(decimal_places = 7, max_digits = 9)
        
    def get_distance(self, point):
        """ Returns the distance in km."""
        lon1, lat1, lon2, lat2 = map(math.radians , [self.lon, self.lat, point.lon, point.lat])
        # haversine formula 
        dlon = lon2 - lon1 
        dlat = lat2 - lat1 
        a = math.sin(dlat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
        c = 2 * math.asin(math.sqrt(a)) 
        km = 6367 * c
        return km
    
    def as_tuple(self):
        """ Returns the tuple (lat, lon). """
        return (self.lat, self.lon)
 
    def get_label(self):
        """ Returns a unique identifier that can be used for textual representation. """
        return "%s,%s" % (self.lat, self.lon)
        
    def __unicode__(self):
        return "%s: %d" % (str(type(self)), self.id)
    
    # Inherit the Meta class from the parent.
    # All points must inherit the Meta class from Point.
    class Meta(Point.Meta):
        pass

class TracePoint(Point):
    """MapPoint with a timestamp when the Point was visited and which video belongs to that Point.
    A TracePoint is like an instance of a MapPoint that belongs to a single single.
    There can be many TracePoints of the same MapPoint, which means the same MapPoint has been visited by many traces.
    """
    video_time_start = models.IntegerField()
    video_time_end = models.IntegerField()
    real_time_start = models.DateTimeField()
    real_time_end = models.DateTimeField()
    video = models.ForeignKey(Video, on_delete = models.CASCADE)
    mappoint = models.ForeignKey(MapPoint, on_delete = models.CASCADE)
    
    def __unicode__(self):
        return "%s: %d-%d" % (str(type(self)), self.mappoint.id, self.video.id) 

#===============================================================================
# Abstract super class
#===============================================================================

class PointConnection(models.Model):
    """ Abstract base class for all connections. It defines a Meta class that stores the name of the app."""
    TP_COST = 1
    MP_COST = 20
    map_source = models.ForeignKey(MapPoint, related_name = "%(app_label)s_%(class)s_map_source_related", on_delete = models.CASCADE)
    map_target = models.ForeignKey(MapPoint, related_name = "%(app_label)s_%(class)s_map_target_related", on_delete = models.CASCADE)
    cost = models.IntegerField()
    mode = models.IntegerField()

    def __unicode__(self):
        return "%s: %d->%d" % (str(type(self)), self.map_source.id, self.map_target.id)

    class Meta:
        # If the app_label is not set, django will not be able to map a model definition to a specific app.
        # As a consequence, django will not load definitions outside of models.py without an app label.
        app_label = appsettings.APP_NAME
        abstract = True

#===============================================================================
# Implementations
#===============================================================================
class MapPointConnection(PointConnection):
    """Connection of two points from a GIS.
    All MapPointConnections form the edges of a the graph that is covered by current video data.
    Every MapPointConnection is unique, i.e. there is exactly one MapPointConnection between two MapPoints.
    """
    pass

class TracePointConnection(PointConnection):
    """Connection of two TracePoints of one trace.
    A TracePointConnection is like an instance of a MapPointConnection that belongs to one trace.
    There can be many TracePoints of the same MapPoint.
    """
    trace_source = models.ForeignKey(TracePoint, related_name = "%(app_label)s_%(class)s_trace_source_related)", on_delete = models.CASCADE)
    trace_target = models.ForeignKey(TracePoint, related_name = "%(app_label)s_%(class)s_trace_target_related", on_delete = models.CASCADE)
    video = models.ForeignKey(Video, related_name = "+", on_delete = models.CASCADE)
