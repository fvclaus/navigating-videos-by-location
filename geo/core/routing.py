'''
Implementation of a modified Dijkstra single-source shortest path algorithm.
The big difference is that it iterates edges instead of nodes.
This is necessary, because there can be multiple edges between any two points.

For a complete explanation, refer to: 'Navigating videos by location' by Mildner et al.
The actual implementation is a bit different than the one described in the paper.
The following details changed:
- There is no MapPointConnection from a MapPoint to itself. The change of traces is handled differently. Refer to _relax.
- Between two MapPoints there is a MapPointConnection, if and only if there is a TracePointConnection between two TracePoints that refer to the MapPoints.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import heapq
from decimal import Decimal
import logging

from django.db import transaction

from geo.models import PointConnection, TracePointConnection, MapPointConnection
from geo.io.graph import can_draw, draw
from geo.models import TracePoint, MapPoint
from geo.base.util import ShortestPathDTO, MapPointConnectionsDTO, ConnectionMode

class Graph:
    """Data structure that hold Points and PointConnections.
    There are only very few public functions.
    The most important one is to insert a trace as a list of TracePointDTO instances.
    Other public functions include drawing the map, calculating the shortest path and changing the mode.
    Everything that starts with an underscore is a private method."""
    MAX_DISTANCE = {ConnectionMode.BIKE : 0.1,
                    ConnectionMode.MOTOR_VEHICLE : 0.5,
                    ConnectionMode.TRAIN:1,
                    ConnectionMode.WALK: 0.05}

    logger = logging.getLogger(__name__)

    def __init__(self, mode):
        mode = int(mode)
        self.set_mode(mode)


    @classmethod
    def get_instance(cls, mode):
        """Caching of instances does not work well with Django ORM.
        This is because TracePoint and MapPoint are referenced by other models.
        Now, because you need to query each individually, you create several instances of 
        the same record. This will lead you into trouble when you attach some attributes to 
        one of this instances and try to access it via another model instance. The instance
        will still hold the old state without the new attribute. This makes for really confusing
        and hard to discover errors."""
        return Graph(mode)

    #===========================================================================
    # MapPoint
    #===========================================================================
    def get_mappoint(self, **kwargs):
        """Returns a MapPoint matched by the keyword argument search or None.
        E.g. lat = Decimal(...), lon = Decimal(...) or pk = 5."""
        point = None
        assert kwargs.get("mode") == None, "Do not use mode. It is added automatically."
        try:
            point = MapPoint.objects.get(mode = self.mode, **kwargs)
        except:
            pass
        return point
    
    def _insert_mappoint(self, lat, lon):
        """Inserts the point as MapPoint if it does not exist yet.
         Returns the new point"""
        self.logger.debug("Adding %s,%s as mappoint." % (lat, lon))
        point = self.get_mappoint(lat = lat, lon = lon)
        if not point:
            point = MapPoint(lat = lat, lon = lon, mode = self.mode) 
            point.save()
        return point

    #====================================================================
    # TracePoint 
    #====================================================================
    def get_tracepoint(self, video, lat = None, lon = None, point = None):
        """Returns a TracePoint or None. Because the video of a trace is unique, you must supply a video.
        The MapPoint can be queried by either giving the lat/lon coordinates or a reference to the MapPoint.
        E.g. video, lat = Decimal(...), lon = Decimal(...) or video, point = MapPoint()."""
        mappoint = None
        if lat and lon:
            mappoint = self.get_mappoint(lat = lat , lon = lon)
        elif point:
            mappoint = point
        tracepoint = None
        try:
            tracepoint = TracePoint.objects.get(mappoint = mappoint, video = video)
        except Exception:
            pass

        return tracepoint
    
    def _insert_tracepoint(self, dto):
        """Inserts the point as TracePoint or updates the time of an existing one and creates a MapPoint point if it does not exist yet.
        Returns the new point."""
        mappoint = self.get_mappoint(lat = dto.lat, lon = dto.lon)

        # Try to get MapPoint from current map insert it if it does not exist
        if not mappoint:
            mappoint = self._insert_mappoint(lat = dto.lat, lon = dto.lon)

        tracepoint = self.get_tracepoint(video = dto.video, point = mappoint)
        if not tracepoint:
            self.logger.debug("Adding %s,%s as tracepoint." % (dto.lat, dto.lon))
            tracepoint = TracePoint(video_time_start = dto.video_time,
                                    video_time_end = dto.video_time,
                                    real_time_start = dto.real_time,
                                    real_time_end = dto.real_time,
                                    video = dto.video,
                                    mode = self.mode,
                                    mappoint = mappoint)
        else:
            self.logger.debug("Updating tracepoint %s." % tracepoint)
            
        # Update the video times of tracepoint. This is necessary when multiple points are matched to the same node.
        if tracepoint.video_time_end <= dto.video_time:
            tracepoint.video_time_end = dto.video_time
            tracepoint.real_time_end = dto.real_time


        if dto.video_time <= tracepoint.video_time_start :
            tracepoint.video_time_start = dto.video_time
            tracepoint.real_time_start = dto.real_time

        tracepoint.save()
        return tracepoint

    #===========================================================================
    # TracePoints
    #===========================================================================
    @transaction.commit_on_success
    def insert_trace(self, trace):
        """Insert all TracePoints in the order of direction.
        A trace is a list of TracePointDTO instances."""
        self._insert_trace(trace)
            
    @transaction.commit_on_success
    def insert_traces(self, traces):
        """Insert all TracePoints in the order of direction."""
        for trace in traces:
            self._insert_trace(trace)
#            self.insert_trace(trace)
        
    def _insert_trace(self, dtos):
        assert len(dtos) > 0, "Must provide dtos."
        old_tracepoint = None
        connection = None
        tracepoints = []
        # Checks if the graph will be partitioned upon inserting this trace.
        # If so, a new MapConnection must be created to close the gap.
        needs_mappoint_connection = MapPoint.objects.all().count() != 0 and \
                MapPoint.objects.filter(lat = dtos[0].lat, lon = dtos[0].lon).count() == 0
        
        for dto_index in range(len(dtos)):
            dto = dtos[dto_index]
            tracepoint = self._insert_tracepoint(dto)

            if old_tracepoint != None and old_tracepoint != tracepoint:
                connection = self._insert_traceconnection(old_tracepoint, tracepoint, dto.video)
            
            if connection != None and needs_mappoint_connection:
                self._connectmappoints(connection)
                needs_mappoint_connection = False
                
            old_tracepoint = tracepoint
            tracepoints.append(tracepoint)
          
    #===========================================================================
    # TracePointConnection
    #===========================================================================
    def _get_traceconnection(self, trace_source, trace_target):
        """Returns the TracePointConnection between the source TracePoint and the target TracePoint."""
        connection = None
        try:
            connection = self._get_traceconnections(trace_source = trace_source, trace_target = trace_target)[0]
        except:
            pass
        return connection
    
    def _insert_traceconnection(self, trace_source, trace_target, video):
        """Inserts a new TracePointConnection and MapPointConnection, if the two points are not connected yet.
         Returns the new TracePointConnection."""
        connection = TracePointConnection(map_source = trace_source.mappoint,
                                   map_target = trace_target.mappoint,
                                   trace_source = trace_source,
                                   trace_target = trace_target,
                                   video = video,
                                   cost = PointConnection.TP_COST,
                                   mode = self.mode)
        connection.save()
        
        if (MapPointConnection.objects.filter(map_source = trace_source.mappoint, 
                                              map_target = trace_target.mappoint).count() == 0):
            self._insert_mapconnection(trace_source.mappoint, trace_target.mappoint, PointConnection.MP_COST)

        return connection
        
    #===========================================================================
    # TracePointConnections
    #===========================================================================
    def _get_traceconnections(self, **kwargs):
        """Returns a list of TracePointConnections matched by the keyword argument search or an empty list.
        E.g. map_source = MapPoint(), map_target = MapPoint(...) returns all TracePointConnections between source and target.
        To get the connection between two TracePoints, refer to _get_traceconnection()."""
        connections = TracePointConnection.objects.filter(**kwargs)
        return connections
    
    #===========================================================================
    # MapPointConnection
    #===========================================================================
    def _insert_mapconnection(self, source, target, cost):
        "Creates a new map connection between two non identical points and returns it"
        connection = MapPointConnection(map_source = source,
                                 map_target = target,
                                 mode = self.mode,
                                 cost = cost)
        connection.save()
        return connection

    def _get_mapconnections(self, **kwargs):
        """Returns a list of MapPointConnections matched by the keyword argument search or an empty list.
        E.g. map_source = MapPoint(), map_target = MapPoint(...) returns all MapPointConnections between source and target."""
        connections = MapPointConnection.objects.filter(**kwargs)
        return connections
    
    #===========================================================================
    # Connections
    #===========================================================================
    def _get_outgoing_connections(self, source):
        """Helper function that returns all outgoing connections. Both TracePointConnection and MapPointConnection."""
        assert isinstance(source, MapPoint), "A TracePoint has max only one outgoing TracePointConnection."
        connections = []
        connections.extend(self._get_traceconnections(map_source = source))
        connections.extend(self._get_mapconnections(map_source = source))
        return connections
    
    def _get_incoming_connections(self, target):
        """Helper function that returns all incoming connections. Both TracePointConnection and MapPointConnection."""
        assert isinstance(target, MapPoint), "A TracePoint has max only one incoming TracePointConnection."
        connections = []
        connections.extend(self._get_traceconnections(map_target = target))
        connections.extend(self._get_mapconnections(map_target = target))
        return connections
    
    #===============================================================================
    # Shortest path
    #===============================================================================
    def calculate_shortest_path(self, source, target):
        """Calculates the shortest path between source and target.
        Returns a sequence of connections (Map or Trace) as ShortestPathDTO."""
        self.logger.info("Starting single source shortest path from %d to %d" % (source.id, target.id))
        (total_cost, predecessor) = self._calculate_shortest_path(source)
        self.logger.info("Finished single source shortest path")
        shortest_path = ShortestPathDTO(graph = self)
        current_connection = None
        max_cost = Decimal("Infinity")
        to_target = self._get_incoming_connections(target)


        self.logger.info("Searching cheapest end connection")
        for connection in to_target:
            if total_cost[connection] < max_cost:
                current_connection = connection
                max_cost = total_cost[connection]

        if len(to_target) == 0 or max_cost == Decimal("Infinity"):
            self.logger.info("Did not find a connection to the target point")
            return shortest_path

        self.logger.info("Found %s with total_cost %s" % (current_connection, str(max_cost)))
        current_point = target
        while current_point != source and current_connection != None:
            shortest_path.add_connection(current_connection)
            current_point = current_connection.map_source
            current_connection = predecessor[current_connection]

        shortest_path.finish()

        return shortest_path

    def _calculate_shortest_path(self, start):
        """Calculates the cost from a single source to any target in the graph.
        Returns total_cost and predecessor as dictionary.
        Total_cost contains the pk of every connection with the total_cost to reach that connection.
        Predecessor contains the pk of every connection with a reference to another predecessor connection.
        It is also possible to add total_cost and predecessor as attributes to every connection instance, but
        this only works as long as none of the other get_connection functions are called.
        Every call to _get_traceconnection for example, returns a new instance (representing the same entry) instead of always returning the same instance.
        This gets very confusing and the easy solution is to store the meta information in a dictionary."""
        # Avoid creating a new array with references to all connections every time.
        connections = self.connections 
        start_connections = self._get_outgoing_connections(start)
        total_cost = ConnectionDict()
        predecessor = ConnectionDict()
        self._init_shortest_path(start_connections, connections, total_cost, predecessor)
        
        visited_connections = []
        # This returns a (shallow)-copy of (the references of) all connections.
        unvisited_connections = connections
        # Visited connections will get popped out of the array.
        # Hence connections does not hold all connections anymore.
        del connections

        while unvisited_connections:

            minimal_cost = [(total_cost[connection], connection) for connection in unvisited_connections]
            heapq.heapify(minimal_cost)

            (cost, connection) = heapq.heappop(minimal_cost)
            # The rest cannot be reached from here
            if total_cost[connection] == Decimal("Infinity"):
                break
            visited_connections.append(connection)
            unvisited_connections.remove(connection)

            successors = self._get_outgoing_connections(connection.map_target)

            for successor in successors:
                self._relax(connection, successor, total_cost, predecessor)
                
        return (total_cost, predecessor)
    
    def _init_shortest_path(self, start_connections, connections, total_cost, predecessor):
        """Set the total_cost of outgoing connections from the start to their cost
        and every other connection to Infinity. Sets all predecessor to None."""
        for connection in start_connections:
            total_cost[connection] = connection.cost
            
        for connection in connections:
            if not total_cost.has_key(connection):
                total_cost[connection] = Decimal("Infinity")

            predecessor[connection] = None
                

    def _relax(self, connection, successor, total_cost, predecessor):
        newcost = total_cost[connection] + successor.cost

        if newcost < total_cost[successor]:
            predecessor[successor] = connection
            total_cost[successor] = newcost
        # If the successor belongs to the same trace as the current connection (the videos are identical)
        # and the total_cost is equal, then change the successor to this trace.
        # This is a simpler implementation for the 'changing-traces-problem'.
        elif newcost == total_cost[successor] and hasattr(successor, "video") and hasattr(connection, "video") and successor.video == connection.video:
            # If there is no extra cost, then it is possible to hop infinitely between two traces covering the same MapPointConnection.
            # Essentially this encourages staying on the same trace for as long as possible.
            predecessor[successor] = connection

    
    #===========================================================================
    # Utility
    #===========================================================================
    def _connectmappoints(self, connection):
        nearest_target = None
        # Do not bridge any distance. 
        # There is a predefined max distance for every mode that is reasonable to connect. 
        shortest_distance = self.MAX_DISTANCE[self.mode]
        # Iterate through every component and try to match two points that are closest to each other.
        for other_connection in MapPointConnection.objects.filter(mode = self.mode):
            target = other_connection.map_target
            distance = connection.map_source.get_distance(target)
            if distance < shortest_distance:
                nearest_target = target
                shortest_distance = distance
        # Bridge the two closest points to eliminate one partition.
        if not nearest_target is None: 
            self._insert_mapconnection(connection.map_source, nearest_target, PointConnection.MP_COST)
        else:
            self.logger.info("Could not find a nearby node for %s." % connection.map_source)

    @property
    def connections(self):
        """Returns all TracePoint and MapPointConnections.
        Very slow. Use with caution. If you want to count, refer to count() in your Django documentation."""
        connections = []
        connections.extend(TracePointConnection.objects.filter(mode = self.mode))
        connections.extend(MapPointConnection.objects.filter(mode = self.mode))
        return connections
    
    def can_draw(self):
        """Tests whether it is possible to draw a graph."""
        return can_draw()

    def draw(self, path, reset = True, draw_tracepoints = True):
        """Refer to io.graph.draw for the documentation."""
        draw(path, MapPoint.objects.filter(mode = self.mode), self.connections, reset, draw_tracepoints)

    
    def as_unique_mapconnections(self):
        """Return the graph as a connection dto consisting of unique, pairwise MapPoint tuples.
        Not route-able, but can be displayed in the frontend as a loose collection of links without a graph data structure."""
        connection_dto = MapPointConnectionsDTO()

        for connection in MapPointConnection.objects.filter(mode = self.mode):
            connection_dto.add_connection(connection)

        return connection_dto

    def set_mode(self, mode):
        if not mode in (ConnectionMode.WALK, ConnectionMode.TRAIN, ConnectionMode.MOTOR_VEHICLE, ConnectionMode.BIKE):
            raise RuntimeError , "Mode " + str(mode) + " should be one of ConnectionMode"
        else:
            self.mode = mode
            
class ConnectionDict():
    """Dictionary that works with TracePoint- and MapPointConnections simultaneously.
    There is a index conflict, when indexing both Connection types in the same standard dictionary, 
    because the hash function returns the primary key of the entry."""
    def __init__(self):
        self.mapconnections = {}
        self.traceconnections = {}
        
    def __getitem__(self, connection):
        if isinstance(connection, MapPointConnection):
            return self.mapconnections[connection.pk]
        else:
            return self.traceconnections[connection.pk]
        
    def __setitem__(self, connection, value):
        if isinstance(connection, MapPointConnection):
            self.mapconnections[connection.pk] = value
        else:
            self.traceconnections[connection.pk] = value
            
    def has_key(self, connection):
        if isinstance(connection, MapPointConnection):
            return self.mapconnections.has_key(connection.pk)
        else:
            return self.traceconnections.has_key(connection.pk)
            
    def __repr__(self):
        return "MapPointConnection: %s\nTracePointConnection: %s" % (str(self.mapconnections), str(self.traceconnections))