'''
Implements a post-processing map-matching algorithm.
If unfamiliar with map-matching, I suggest reading 'Current Map Matching Algorithms for Transport Applications: State-of-
the art and Future Research Directions' by Mohammed A Quddus. It provides a good introduction.

The algorithm implemented in this module is topological algorithm and makes use of some features of the algorithm from 'Developing an Enhanced Weight-Based Topological Map-
Matching Algorithm for Intelligent Transport Systems by Velaga et al'.
Points are matched using a point-to-link metric with two weighted coefficients that are taken from the afore mentioned paper.

To further enhance the accuracy, a derivation of back-jumped based backtracking, known in the domain of artificial intelligence, has been implemented.
In the following points are coordinates from an external source like a .gpx file that needs to be mapped.
Nodes are coordinates in a GIS like OSM and links connect two nodes.
Basically the algorithm works like this:
- Select the best matching link for the current point using the two different weighted coefficients.
- Select the source node of the link as current node.
- If the predecessor node is equal to the current node and continue with the next point.
- If the current node is adjacent to the predecessor, _save the current state and mark the current node as predecessor and continue with the next point.
- If the current node is not adjacent to the predecessor, _restore to  the state right before the predecessor was marked as the current node and blacklist the connection used to travel to this predecessor node and continue with the same point.
- Terminate when all points are mapped to nodes.

The motivation behind the back-jumping is the following common scenario:
x: Point, <number>: Node, | & -: connection

  6  x
  |
  5   x
  |
4-2-3 x x (one outlier)
  |
  1 x

In this example, to the algorithm, it looks like that the trace wants to make a left-turn (to 3) at the junction (4).
It is only possible to recognize this mistake when the trace continues on the vertical road.
Because successive points match to 5 and 5 is not adjacent to 3 the state shortly before 3 was reached will be restored.
The state shortly before 3 was reached is following:
- Two points have been matched: The first one to 1 the second one to 2.
- The current node is 2
- The predecessor node is 1
- The current point is the third point (right of the second point)
After restoring to the previous state, the link between 2-3 will be added to the blacklist.
The third point will therefore be matched to 5, because it cannot reach 3 anymore.

There are more complicated scenarios that this algorithm solves. The algorithm has the following properties:
- It assumes that the next points are correct (i.e. they are not outliers) and that already matched points are matched wrong.
- It supports n backjumps (n is the number of matched points).
- If the last point matches correctly, all points will be matched correctly (this has not been proven).


Furthermore, there is a small consistency check that checks if the trace is stuck on a certain link.
Consider the following scenario that happens frequently when walking on the pavement:

5   x
|   x
4   x
|   x 
3-2-1

Because the heading is identical for 1-2 and 2-3, 1-2 will always be preferred because of its closer distance.
3-4, 4-5 would be a better match, but cannot be reached yet (topological matching!)

The solution is to blacklist the current link, if the points are moving away from all(!) nodes.
This will force the matching to move forward. 
There is also a small check that avoids blacklisting, if points are scattered around a node (e.g. at a red light).

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import os, math
from copy import copy

import logging
from datetime import timedelta
from xml.dom import minidom

from django.conf import settings


from geo.io.osm import OSMGraph
from geo.base.vector import subtract_vectors, scalar_multiply_vector, add_vectors, normalize_vector, dot_product
from geo.base.exceptions import OSMException
from geo.base.util import ConnectionMode 
    
logger = logging.getLogger(__name__)
        

class TopologicalMapMatcher():
    """Superclass for all topological map-matchers.
    Every subclass must load its own graph data from some source and store it as self.graph.
    The interface for the graph is current defined in geo.io.osm."""
    def __init__(self):
        self.state = {}
        
    def match(self, coordinates, interpolation_factor = 0):
        """Map matches the input coordinates/points and returns the same number of map-matched points as <lat, lon, time, node_id>.
        If the interpolation_factor is bigger than 0, the number of new points will be interpolated between every two points of the input coordinates."""
        index = 0
        finished = False

        if interpolation_factor > 0:
            self._do_linear_interpolation(coordinates, interpolation_factor)
        n_coordinates = len(coordinates)
        assert n_coordinates > 1
        headings = self._calculate_headings(coordinates)
        old_distances = []
        blacklist = set()
        (predecessors, predecessor_links) = ([], [])
        (current_link, predecessor_link) = (None, None)
        logger.debug("NEW MAP-MATCHING START\n")
                
        while not finished:
            coordinate = coordinates[index]
            logger.debug("Getting coordinate %s" % str(coordinate))
            # Map match the first point using the heading to the second point.
            if len(predecessors) == 0:
                logger.debug("Start of the trace. No other coordinate looked at yet.")
                (current, current_link, distances) = self._graph_match(coordinate, subtract_vectors(coordinate, coordinates[1]))
                self.view.add(current)
        
            else:
                predecessor = predecessors[-1]
                predecessor_link = predecessor_links[-1]
                try:
                    # Calculate the current heading/direction.
                    if index < n_coordinates - 1:
                        direction = subtract_vectors(coordinate, coordinates[index + 1])
                    else:
                        direction = subtract_vectors(coordinates[index - 1], coordinate)
                        
                    (current, current_link, distances) = self._subgraph_match(coordinate, direction, blacklist)
                    # Matching reached a new node.
                    if current != predecessor:
                        # The new node is a direct successor.
                        if predecessor.is_adjacent(current):
                            logger.debug("Saving current state.")
                            self._save(current, {"index": index,
                                       "predecessors": copy(predecessors),
                                       "predecessor_links": copy(predecessor_links),
                                       "current" : predecessor})
                            # Extend the view with the new nodes/links that can be reached now.
                            self.view.add(current, predecessor)
                            # Blacklist the link that matched the predecessor node and make it impossible to go back to the predecessor (on the same link).
                            # This is necessary and will prevent outliers from destroying the quality of the matching.
                            blacklist.add(predecessor_link)
                        # The new node is not adjacent, matching made a mistake before.
                        else:
                            logger.debug("Node %s is not adjacent to node %s." % (current.id, predecessor.id))
                            state = self._restore(predecessor)
                            # The predecessor was not the right node. Backlist the predecessor link.
                            blacklist.add(predecessor_link)
                            
                            current = state["current"]
                            predecessors = state["predecessors"]
                            predecessor_links = state["predecessor_links"]
                            predecessor_link = predecessor_links[-1]
                            predecessor = predecessors[-1]
                            # Allow to match the pre-predecessor's link.
                            blacklist.remove(predecessor_link)
                            index = state["index"]
                            continue
                    else:
                        # Check if the matching is stuck somewhere see module docs for explanation.
                        if headings[index] > math.cos(math.pi / 4):
                            if self._is_nodes_moving_away(old_distances, distances):
                                logger.debug("Traces seems to be heading in an unknown direction. Adding %s to blacklist." % (current.id))
                                blacklist.add(current_link)
                                continue
                # This expection will be raised if all(!) links are blacklisted.
                # TODO Can this really happen or is this just a programming error?
                except OSMException, e:
                    raise e
    
            index += 1
            logger.debug("Appending %s." % current.id)
            predecessors.append(current)  # everything is fine
            predecessor_links.append(current_link)
            
            old_distances = distances
    
            if index >= n_coordinates:
                finished = True
                
        return [(node.lat, node.lon, coordinate[2], node.id) for (node, coordinate) in zip(predecessors, coordinates)]
    
    def _do_linear_interpolation(self, coordinates, factor):
        # Number of sampling points the current connection should have after interpolation
        # If the factor is 2, there should be 3 sampling points 1/3, 2/3 and 3/3
        n_sampling_points = factor + 1
        # Total number of newly interpolated coordinates
        n_new_coordinates = 0
        for index in range(1, len(coordinates)):
            # New coordinates have been inserted, increment the index to select the correct source and target.
            coordinate_index = index + n_new_coordinates
            a = coordinates[coordinate_index - 1]
            b = coordinates[coordinate_index]
            a_to_b = subtract_vectors(a, b)
            for sampling_index in range(1, n_sampling_points):
                # The current sampling point, e.g. 2/3
                sampling_point = float(sampling_index) / n_sampling_points
                a_to_b_segment = scalar_multiply_vector(a_to_b, sampling_point)
                # The index for the newly sampled coordinate.
                new_coordinate_index = index + n_new_coordinates
                new_coordinate = add_vectors(a, a_to_b_segment)
                time_difference = b[2] - a[2]
                # Interpolate time.
                new_time = a[2] + timedelta(int(time_difference.days * sampling_point),
                                            int(time_difference.seconds * sampling_point),
                                            int(time_difference.microseconds * sampling_point))
                coordinates.insert(new_coordinate_index, (new_coordinate[0], new_coordinate[1], new_time))
                n_new_coordinates += 1
    
    def _calculate_headings(self, coordinates):
        """Calculates the heading (= dot product) for every point. This is an o.k. approximation for gyroskop values."""
        headings = []
        for index in range(len(coordinates)):
            if index < 2:
                headings.append(1.0)
            else:
                headings.append(self._calculate_heading(coordinates[index - 2],
                                                         coordinates[index - 1],
                                                         coordinates[index]))
        return headings
    
    def _calculate_heading(self, a, b, c):
        a_to_b = subtract_vectors(a, b)
        b_to_c = subtract_vectors(b, c)
        a_to_b_unit = normalize_vector(a_to_b)
        b_to_c_unit = normalize_vector(b_to_c)
        return dot_product(a_to_b_unit, b_to_c_unit)
    
    
    def _is_nodes_moving_away(self, old_distances, distances):
        """Checks if the distances to all links is increasing compared to the previous iteration."""
        is_longer = True
        if len(old_distances) != len(distances):
            return False
        for index in range(len(old_distances)):
            if old_distances[index] > distances[index]:
                is_longer = False
                break
        return is_longer
    
    def _save(self, jumped_to, state):
        """Creates a checkpoint that can be restored later to correct false matching.
        A single node can be visited multiple times and can therefore have multiple checkpoints."""
        save_id = jumped_to.id
        if self.state.has_key(save_id):
            self.state[save_id].append((state, self.view.copy()))
        else:
            self.state[save_id] = [(state, self.view.copy())]
        
    def _restore(self, back_to):
        """Restores the latest checkpoint that has been created for this node."""
        state_store = self.state[back_to.id]
        (state, self.view) = state_store.pop()
        return state
        
    
    def _graph_match(self, coordinate, direction):
        """Match the coordinate on the whole graph without any context."""
        self.view = TopologicalView()
        return self.graph.graph_match(coordinate, direction)
    
    def _subgraph_match(self, coordinate, direction, blacklist):
        """Match the coordinate topologically."""
        links = self.view.as_links(blacklist)
        return self.graph.subgraph_match(coordinate, direction, links)


class TopologicalView():
    """Models the topological view.
    It stores all links and nodes that are currently visible."""
    def __init__(self):
        self.links = set()
                
    def copy(self):
        """Make a deep copy of this view and return a reference."""
        other = TopologicalView()
        # Array of references so shallow copy is enough.
        other.links = copy(self.links)
        return other
    
    def add(self, current, predecessor = None):
        # Do not add the link to the predecessor. Going back the other direction is currently not supported. 
        self.links.update([link for link in current.links if not link.target is predecessor])
        logger.debug("Adding node %s." % current)
        # Exclude the predecessor.
        for successor in current.successors:
            if successor is predecessor:
                continue
            logger.debug("Adding neighbour %s." % successor)
            # Exclude links to the new node. They should not be matched, because they cannot be reached atm.
            self.links.update([link for link in successor.links if not link.target is current])
    
    def as_links(self, blacklist):
        """Returns all links that are currently visible and not blacklisted."""
        links = []
        for link in self.links:
            if link in blacklist:
                continue
            links.append(link)
        return links


class OSM(TopologicalMapMatcher):
    """Map-matching on OSM nodes and links. 
    Depending on the transportation mode loads .osm files from the res directory.
    The .osm files only contain the 'Squares' in Mannheim, Germany."""
    # TODO Needs to accept a reference to .osm file
    def __init__(self, mode):
        TopologicalMapMatcher.__init__(self)
        self.mode = mode
        #TODO
        doc = minidom.parse(os.path.join(settings.MAP_DIR, "%d.osm" % mode))
        ways = doc.getElementsByTagName("way")
        nodes = doc.getElementsByTagName("node")
        # There are no one-way streets for pedestrians, but there is only a link in one direction.
        # For most streets there is only one link in one direction too, although it is not a one-way street.
        # One way streets in OSM are marked with a special attribute.
        if mode != ConnectionMode.TRAIN:
            bidirectional = True
        else:
            bidirectional = False
            
        self.graph = OSMGraph(nodes, ways, bidirectional = bidirectional)