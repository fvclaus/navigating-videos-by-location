'''
Helper functions that operate on results of some calculation.
These functions have been aggregated to data transfer objects (DTO's).

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import os
import json

from django.conf import settings

class ConnectionMode:
    WALK = 0
    TRAIN = 1
    BIKE = 2
    MOTOR_VEHICLE = 3

    ALL = [WALK, BIKE, TRAIN, MOTOR_VEHICLE]
    

class ShortestPathDTO:
    """Data transfer object for the result of a path calculation.
    This was created to encapsulate helper functions that operate on the shortest path data."""
    def __init__(self, graph):
        self.connections = []
        self.graph = graph
        
    def add_connection(self, connection):
        """Connections must be added one at a time in order or in reverse order. Refer to finish()."""
        self.connections.append(connection)
        
    def finish(self):
        """If the connections are added in reverse order call finish() to reverse the connections as a last step."""
        self.connections.reverse()

    def as_unique_mapconnections(self):
        """Returns a MapPointConnectionsDTO that can be used to generate a .gpx file to display the shortest path on a map. Refer to GPX"""
        point_trace = MapPointConnectionsDTO()
        if not self.connections:
            return point_trace
        point_trace.add_point(self.connections[0].map_source)
        for connection in self.connections:
            point_trace.add_point(connection.map_target)
        return point_trace

    def as_json(self):
        """Returns the path as json data consisting of a sequence of tuples that hold connection.source and connection.target.
        Each holds all necessary attributes like video_time_start and node id's of the GIS."""
        data = []
        
        for connection in self.connections:
            data.append(self._connection_to_point(connection.map_source, connection, self.graph))
        
        if self._last_connection:
            data.append(self._connection_to_point(self._last_connection.map_target, self._last_connection, self.graph))
        return json.dumps(data, indent = 4)
    
    @property
    def _last_connection(self):
        if self.connections:
            return self.connections[-1]
        else:
            return None
    
    def _connection_to_point(self, point, connection, graph):
        jpoint = {
                  "lat" : str(point.lat),
                  "lon" : str(point.lon),
                  "id" : str(point.id)}
        
        # Check if this is a TracePointConnection.
        # Importing from models causes an import cycle.
        if hasattr(connection,"video"):
            tracepoint = graph.get_tracepoint(video = connection.video , point = point)
            assert tracepoint, "Must get a tracepoint for point %s with video %d." % (point, connection.video.id)
    
            jpoint["video_time_start"] = tracepoint.video_time_start
            jpoint["video_time_end"] = tracepoint.video_time_end
            jpoint["src"] = tracepoint.video.path
        return jpoint

    def __str__(self):
        if not self.connections:
            return ""
        connections = []
        connections.append(str(self.connections[0].map_source.id))
        for connection in self.connections:
            label = str(connection.map_target.id)
            if hasattr(connection, "video"):
                label = "(%d)%d" % (connection.video.id, connection.map_target.id)
            connections.append(label)
        return ">".join(connections)

class MapPointConnectionsDTO:
    """Models an arbitrary trace consisting of a sequence of MapPoints.
    This was created to """
    def __init__(self):
        self.connections = []
        
    def add_connection(self, connection):
        self.connections.append([connection.map_source, connection.map_target])
        
    def as_json(self):
        data = []
        for connection in self.connections:
            pair = []
            for mappoint in connection:
                pair.append({"lat" : str(mappoint.lat),
                             "lon" : str(mappoint.lon),
                             "id" : mappoint.id})
            data.append(pair)
            
        return json.dumps(data, indent = 4)
    
    def __iter__(self):
        for connection in self.connections:
            yield connection
    
    def __len__(self):
        return len(self.connections)
            

class TracePointDTO:
    """Data transfer object for TracePoint.
    Use this to insert points from an external source in a Graph.
    Access attributes directly. If necessary add a property later."""
    def __init__(self, lat, lon, real_time, video_time, video = None):
        self.lat = lat
        self.lon = lon
        self.real_time = real_time
        self.video_time = video_time
        self.video = video

    def __str__(self):
        return "%s, %s" % (self.lat, self.lon)
    def __repr__(self):
        return self.__str__()
