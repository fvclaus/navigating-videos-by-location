'''

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

class OSMException(Exception): 
    pass

class OggException(Exception):
    pass

class NoTrkTagException(Exception):
    pass

class NoTrkSegTagException(Exception):
    pass

class NoTrkPtException(Exception):
    pass

class InsertTracePointException(Exception):
    pass
