'''
Contains mathematical operations on 2-dimensional vectors.

@author: Frederik Claus <f.v.claus@googlemail.com>
'''

import math
from decimal import Decimal

def subtract_vectors(a, b):
    return (b[0] - a[0], b[1] - a[1])

def add_vectors(a, b):
    return (a[0] + b[0], a[1] + b[1])

def normalize_vector(vector):
    length = Decimal(math.sqrt(vector[0] ** 2 + vector[1] ** 2))
    return (vector[0] / length, vector[1] / length)
 
def dot_product(a, b):
    return (a[0] * b[0] + a[1] * b[1])

def scalar_multiply_vector(vector, scalar):
    scalar_as_decimal = Decimal(scalar)
    return (vector[0] * scalar_as_decimal, vector[1] * scalar_as_decimal)

def calculate_distance(coord1, coord2):
    """ Calculates distance between to geographic coordinates in km."""
    lat1, lon1 = (coord1[0], coord1[1])
    lat2, lon2 = (coord2[0], coord2[1])
    lon1, lat1, lon2, lat2 = map(math.radians , [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = math.sin(dlat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
    c = 2 * math.asin(math.sqrt(a)) 
    km = 6367 * c
    return km